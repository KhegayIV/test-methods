package com.selenium.test.utils;

import javax.mail.*;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import static java.lang.Thread.sleep;

public class MailUtil {

    public static String getNewPasswordFromEmail()//String protocol, String email, String password)
    {
        String newPassword = "";
        Properties props = new Properties();
        props.setProperty("mail.store.protocol", "imaps");
        try{
            sleep(10000);
            Session session = Session.getInstance(props, null);
            Store store = session.getStore();
            store.connect("imap.mail.ru", "stz_lab@mail.ru", "myVeryStrongPassword1664");
            Folder inbox = store.getFolder("inbox");
            inbox.open(Folder.READ_ONLY);
            int count = inbox.getMessageCount();
            int startCount = (count < 10) ? 1 : count-10;
            Message[] msgs = inbox.getMessages(startCount, count);
            System.out.println("get mesgs");
            Message[] tvtropesMsgs = Arrays.stream(msgs).filter(m -> {
                try {
                    return m.getSubject().equals("TV Tropes Password Reset") && m.getFrom()[0].toString().equals("thestaff@tvtropes.org");
                } catch (MessagingException e) {
                    e.printStackTrace();
                    return false;
                }
            }).sorted((m1, m2) -> {
                try {
                    return m2.getSentDate().compareTo(m1.getSentDate());
                } catch (MessagingException e) {
                    e.printStackTrace();
                    return 0;
                }
            }).toArray(Message[]::new);
            Message msg = tvtropesMsgs[0];
            System.out.println(msg.getSentDate().toString());
            String content = msg.getContent().toString();
            String search = "Your password has been set to: ";
            int passStartPos = content.lastIndexOf(search) + search.length();
            String password = content.substring(passStartPos, passStartPos+8);
            newPassword = password;
        } catch (Exception e){
        }

        return newPassword;
    }
}
