package com.selenium.test.junit.tests.Logon;

import com.codeborne.selenide.Condition;
import com.selenium.test.tvtropes.pages.*;
import com.selenium.test.utils.MailUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.support.ui.Wait;


import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.Selenide.sleep;
import static org.junit.Assert.*;

public class TVTropsLogonTest {
    private static final Logger LOG = LogManager.getLogger("TVTropsLogonTests");

    @Test
    public void testAuthorizationCorrect(){
        System.setProperty("webdriver.chrome.driver", "D:\\WORK\\PROG\\Web_customer\\met_test\\ChromeDriver\\chromedriver.exe");
        System.setProperty("selenide.browser", "chrome");
        String startUrl = "http://tvtropes.org/";
        TVTropesStartPage startPage = open(startUrl, TVTropesStartPage.class);
        LOG.info(String.format("Open %s", startUrl));
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();
        logonPage.insertUserCredentials("solitarymite", "testPassword1994");
        startPage = logonPage.logon();
        assertTrue(startPage.isAuthenticated());
        startPage.logOut();
    }

    @Test
    public void testAuthorizationWrongPasswordFail(){
        System.setProperty("webdriver.chrome.driver", "D:\\WORK\\PROG\\Web_customer\\met_test\\ChromeDriver\\chromedriver.exe");
        System.setProperty("selenide.browser", "chrome");

        TVTropesStartPage startPage = open("http://tvtropes.org/", TVTropesStartPage.class);
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();
        logonPage.insertUserCredentials("solitarymite", "aludfygbvla");
        startPage = logonPage.logon();
        assertFalse(logonPage.isAuthorizationCorrect());
    }

    @Test
    public void testAuthorizationRetrievePasswordInvalideEmailFormatFail(){
        System.setProperty("webdriver.chrome.driver", "D:\\WORK\\PROG\\Web_customer\\met_test\\ChromeDriver\\chromedriver.exe");
        System.setProperty("selenide.browser", "chrome");

        TVTropesStartPage startPage = open("http://tvtropes.org/", TVTropesStartPage.class);
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();
        logonPage.insertUserCredentials("solitarymite", "aludfygbvla");
        logonPage.logon();
        TVTropesRetrievePasswordPage retrievePasswordPage = logonPage.resetPassClick();
        retrievePasswordPage.insertUserCredentials("solitarymite", "voaytrefvbauerv");
        retrievePasswordPage.resetPasswordClick();
        assertFalse(retrievePasswordPage.isEmailValide());
    }

    @Test
    public void testAuthorizationRetrievePasswordWrongEmailFail(){
        System.setProperty("webdriver.chrome.driver", "D:\\WORK\\PROG\\Web_customer\\met_test\\ChromeDriver\\chromedriver.exe");
        System.setProperty("selenide.browser", "chrome");

        TVTropesStartPage startPage = open("http://tvtropes.org/", TVTropesStartPage.class);
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();
        logonPage.insertUserCredentials("solitarymite", "aludfygbvla");
        logonPage.logon();
        TVTropesRetrievePasswordPage retrievePasswordPage = logonPage.resetPassClick();
        retrievePasswordPage.insertUserCredentials("solitarymite", "voaytrefvbauerv@mail.ru");
        retrievePasswordPage.resetPasswordClick();
        assertFalse(retrievePasswordPage.isResetFormCorrect());
    }

    @Test
    public void testAuthorizationRetrieveUserNameWrongEmailFail(){
        System.setProperty("webdriver.chrome.driver", "D:\\WORK\\PROG\\Web_customer\\met_test\\ChromeDriver\\chromedriver.exe");
        System.setProperty("selenide.browser", "chrome");

        TVTropesStartPage startPage = open("http://tvtropes.org/", TVTropesStartPage.class);
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();
        TVTropesRetrieveUserNamePage retrieveUserNamePage = logonPage.retrieveUserNameClick();

        retrieveUserNamePage.insertUserCredentials("apyfbvlahbriu7grgbvf@mail.ru");
        retrieveUserNamePage.resetUserNameClick();
        assertTrue(retrieveUserNamePage.isWrongEmailErrorOccure());
    }

    @Test
    public void testAuthorizationRetrieveUserNameEpmtyEmailFail(){
        System.setProperty("webdriver.chrome.driver", "D:\\WORK\\PROG\\Web_customer\\met_test\\ChromeDriver\\chromedriver.exe");
        System.setProperty("selenide.browser", "chrome");

        TVTropesStartPage startPage = open("http://tvtropes.org/", TVTropesStartPage.class);
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();

        TVTropesRetrieveUserNamePage retrieveUserNamePage = logonPage.retrieveUserNameClick();

        retrieveUserNamePage.resetUserNameClick();
        assertTrue(retrieveUserNamePage.isEmptyEmailErrorOccure());
    }

    @Test
    public void testAuthorizationRetrievePasswordValideEmailCorrect(){
        System.setProperty("webdriver.chrome.driver", "D:\\WORK\\PROG\\Web_customer\\met_test\\ChromeDriver\\chromedriver.exe");
        System.setProperty("selenide.browser", "chrome");

        TVTropesStartPage startPage = open("http://tvtropes.org/", TVTropesStartPage.class);
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();
        logonPage.insertUserCredentials("solitarymite", "skjtgbuorbtguetxd");
        logonPage.logon();
        TVTropesRetrievePasswordPage retrievePasswordPage = logonPage.resetPassClick();
        retrievePasswordPage.insertUserCredentials("solitarymite", "stz_lab@mail.ru");
        retrievePasswordPage.resetPasswordClick();
        String newPassword = MailUtil.getNewPasswordFromEmail();
        logonPage = startPage.clickLogonIcon();
        logonPage.insertUserCredentials("solitarymite", newPassword);
        startPage = logonPage.logon();
        assertTrue(startPage.isAuthenticated());
        startPage.changePassword(newPassword, "testPassword1994");
        startPage.logOut();
    }
}
