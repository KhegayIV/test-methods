package com.selenium.test.tvtropes.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

public class WatchPage {
    @FindBy(xpath = "//a[contains(text(), 'Home Page')]")
    private SelenideElement homePageElement;

    public SelenideElement getHomePageElement() {
        return homePageElement;
    }
}
