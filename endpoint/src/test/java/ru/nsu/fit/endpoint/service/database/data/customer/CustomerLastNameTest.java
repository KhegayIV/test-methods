package ru.nsu.fit.endpoint.service.database.data.customer;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.exceptions.Customer.CustomerNameException;
import ru.nsu.fit.endpoint.service.database.exceptions.Customer.CustomerNullFieldException;
import ru.nsu.fit.endpoint.service.database.data.Customer;


public class CustomerLastNameTest extends CustomerTestBase{

    @Test
    public void testNull() {
        expectedEx.expect(CustomerNullFieldException.class);
        expectedEx.expectMessage("Last name should be specified");
        new Customer(validFirstName, null, validLogin, validPassword, validMoney);
    }

    @Test
    public void testShort() {
        expectedEx.expect(CustomerNameException.class);
        expectedEx.expectMessage("Last name's length should be 2 symbols or more");
        new Customer(validFirstName, "S", validLogin, validPassword, validMoney);
    }

    @Test
    public void testShortEnough() {
        new Customer(validFirstName, "Sm", validLogin, validPassword, validMoney);
    }

    @Test
    public void testLong() {
        expectedEx.expect(CustomerNameException.class);
        expectedEx.expectMessage("Last name's length should be 12 symbols or less");
        new Customer(validFirstName, "Smittersonons", validLogin, validPassword, validMoney);
    }

    @Test
    public void testLongEnough() {
        new Customer(validFirstName, "Smittersonio", validLogin, validPassword, validMoney);
    }

    @Test
    public void testSmallLetter(){
        expectedEx.expect(CustomerNameException.class);
        expectedEx.expectMessage("Last name has invalid format");
        new Customer(validFirstName, "smith", validLogin, validPassword, validMoney);
    }

    @Test
    public void testBigLetter(){
        expectedEx.expect(CustomerNameException.class);
        expectedEx.expectMessage("Last name has invalid format");
        new Customer(validFirstName, "SmitH", validLogin, validPassword, validMoney);
    }

    @Test
    public void testIllegalSymbols(){
        expectedEx.expect(CustomerNameException.class);
        expectedEx.expectMessage("Last name has invalid format");
        new Customer(validFirstName, "Smith22", validLogin, validPassword, validMoney);
    }

}
