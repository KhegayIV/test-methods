package ru.nsu.fit.endpoint.service.database.data.user;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.exceptions.User.UserNameException;
import ru.nsu.fit.endpoint.service.database.exceptions.User.UserNullFieldException;
import ru.nsu.fit.endpoint.service.database.data.User;


public class UserLastNameTest extends UserTestBase{

    @Test
    public void testNull() {
        expectedEx.expect(UserNullFieldException.class);
        expectedEx.expectMessage("Last name should be specified");
        new User(validFirstName, null, validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testShort() {
        expectedEx.expect(UserNameException.class);
        expectedEx.expectMessage("Last name's length should be 2 symbols or more");
        new User(validFirstName, "S", validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testShortEnough() {
        new User(validFirstName, "Sm", validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testLong() {
        expectedEx.expect(UserNameException.class);
        expectedEx.expectMessage("Last name's length should be 12 symbols or less");
        new User(validFirstName, "Smittersonons", validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testLongEnough() {
        new User(validFirstName, "Smittersonio", validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testSmallLetter(){
        expectedEx.expect(UserNameException.class);
        expectedEx.expectMessage("Last name has invalid format");
        new User(validFirstName, "smith", validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testBigLetter(){
        expectedEx.expect(UserNameException.class);
        expectedEx.expectMessage("Last name has invalid format");
        new User(validFirstName, "SmitH", validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testIllegalSymbols(){
        expectedEx.expect(UserNameException.class);
        expectedEx.expectMessage("Last name has invalid format");
        new User(validFirstName, "Smith22", validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

}
