package ru.nsu.fit.endpoint.service.database.data.serviceplan;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanException;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanFeePerUnitRangeException;

import static org.junit.Assert.assertTrue;

public class ServicePlanFeePerUnitTests
{
    @Test(expected = ServicePlanFeePerUnitRangeException.class)
    public void testServicePlanTooLittleFeePerUnit()
    {
        new Plan("name", "details", 2, 1, -1, 0);
    }

    @Test(expected = ServicePlanFeePerUnitRangeException.class)
    public void testServicePlanTooBigFeePerUnit()
    {
        new Plan("name", "details", 2, 1, 1000000, 0);
    }

    @Test()
    public void testServicePlanFeePerUnitMinValue()
    {
        boolean isCorrect = true;

        try{
            new Plan("name", "details", 2, 1, 0, 0);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }

    @Test()
    public void testServicePlanFeePerUnitMaxValue()
    {
        boolean isCorrect = true;

        try{
            new Plan("name", "details", 2, 1, 999999, 0);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }
}
