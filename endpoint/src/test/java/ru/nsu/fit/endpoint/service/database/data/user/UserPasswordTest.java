package ru.nsu.fit.endpoint.service.database.data.user;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.exceptions.User.UserInvalidPassException;
import ru.nsu.fit.endpoint.service.database.exceptions.User.UserNullFieldException;
import ru.nsu.fit.endpoint.service.database.data.User;

/**
 * Created on 09.10.2016.
 */
public class UserPasswordTest extends UserTestBase{

    @Test
    public void testNull() {
        expectedEx.expect(UserNullFieldException.class);
        expectedEx.expectMessage("Password should be specified");
        new User(validFirstName, validLastName, validLogin, null, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testShort() {
        expectedEx.expect(UserInvalidPassException.class);
        expectedEx.expectMessage("Password's length should be 6 symbols or more");
        new User(validFirstName, validLastName, validLogin, "pwd", validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testShortEnough() {
        new User(validFirstName, validLastName, validLogin, "paswrd", validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testLong() {
        expectedEx.expect(UserInvalidPassException.class);
        expectedEx.expectMessage("Password's length should be 12 symbols or less");
        new User(validFirstName, validLastName, validLogin, "thisisaveryveryverylongpassword", validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testLongEnough() {
        new User(validFirstName, validLastName, validLogin, "barelyenough", validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testEasyPass(){
        expectedEx.expect(UserInvalidPassException.class);
        expectedEx.expectMessage("Password is too easy");
        new User(validFirstName, validLastName, validLogin, "qwerty11123", validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testFirstName(){
        expectedEx.expect(UserInvalidPassException.class);
        expectedEx.expectMessage("First name can't be part of a password");
        new User("John", validLastName, validLogin, "John'sPass", validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testLastName(){
        expectedEx.expect(UserInvalidPassException.class);
        expectedEx.expectMessage("Last name can't be part of a password");
        new User(validFirstName, "Smith", validLogin, "IAmSmith", validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testLogin(){
        expectedEx.expect(UserInvalidPassException.class);
        expectedEx.expectMessage("Login can't be part of a password");
        new User(validFirstName, validLastName, "a@b.cd", "a@b.cd1234", validUserRole, validCustomerId, validSubscriptionIds);
    }
}
