package ru.nsu.fit.endpoint.service.database.data.user;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.data.User;

/**
 * Created on 09.10.2016.
 */
public class UserTestValid extends UserTestBase {
    @Test
    public void testValid() {
        new User(validFirstName, validLastName, validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }
}
