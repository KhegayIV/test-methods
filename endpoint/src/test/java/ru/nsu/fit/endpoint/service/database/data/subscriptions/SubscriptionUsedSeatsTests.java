package ru.nsu.fit.endpoint.service.database.data.subscriptions;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.enums.SubscriptionStatus;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanException;
import ru.nsu.fit.endpoint.service.database.exceptions.Subscription.SubscriptionIllegalUsedSeatsException;
import ru.nsu.fit.endpoint.service.database.exceptions.Subscription.SubscriptionUsedSeatsRangeException;

import java.util.UUID;

import static org.junit.Assert.assertTrue;

public class SubscriptionUsedSeatsTests
{
    @Test(expected = SubscriptionUsedSeatsRangeException.class)
    public void testSubscriptionTooLittleUsedSeats() {
        new Subscription(UUID.randomUUID(), UUID.randomUUID(), 2, 1, -1, SubscriptionStatus.DONE_STATUS);
    }

    @Test(expected = SubscriptionUsedSeatsRangeException.class)
    public void testSubscriptionTooBigUsedSeats() {
        new Subscription(UUID.randomUUID(), UUID.randomUUID(), 2, 1, 1000000, SubscriptionStatus.DONE_STATUS);
    }

    @Test(expected = SubscriptionIllegalUsedSeatsException.class)
    public void testSubscriptionMaxSeatsSmallerThenUsedSeats(){
        new Subscription(UUID.randomUUID(), UUID.randomUUID(), 2, 1, 3, SubscriptionStatus.DONE_STATUS);
    }

    @Test()
    public void testSubscriptionUsedSeatsMinValue() {
        boolean isCorrect = true;

        try {
            new Subscription(UUID.randomUUID(), UUID.randomUUID(), 1, 1, 0, SubscriptionStatus.DONE_STATUS);
        } catch (ServicePlanException e) {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }

    @Test()
    public void testSubscriptionUsedSeatsMaxValue() {
        boolean isCorrect = true;

        try {
            new Subscription(UUID.randomUUID(), UUID.randomUUID(), 999999, 1, 999999, SubscriptionStatus.DONE_STATUS);
        } catch (ServicePlanException e) {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }
}
