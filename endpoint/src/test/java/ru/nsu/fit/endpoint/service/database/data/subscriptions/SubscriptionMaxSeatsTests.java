package ru.nsu.fit.endpoint.service.database.data.subscriptions;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.enums.SubscriptionStatus;
import ru.nsu.fit.endpoint.service.database.exceptions.IllegalSeatsException;
import ru.nsu.fit.endpoint.service.database.exceptions.MaxSeatsRangeException;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanException;

import java.util.UUID;

import static org.junit.Assert.assertTrue;

public class SubscriptionMaxSeatsTests
{
    @Test(expected = MaxSeatsRangeException.class)
    public void testSubscriptionTooLittleMaxSeats()
    {
        new Subscription(UUID.randomUUID(), UUID.randomUUID(), 0, 1, 0, SubscriptionStatus.DONE_STATUS);
    }

    @Test(expected = MaxSeatsRangeException.class)
    public void testSubscriptionTooBigMaxSeats()
    {
        new Subscription(UUID.randomUUID(), UUID.randomUUID(), 1000000, 1, 0, SubscriptionStatus.DONE_STATUS);
    }

    @Test(expected = IllegalSeatsException.class)
    public void testSubscriptionMaxSeatsSmallerThenMinSeats()
    {
        new Subscription(UUID.randomUUID(), UUID.randomUUID(), 1, 2, 0, SubscriptionStatus.DONE_STATUS);
    }

    @Test()
    public void testSubscriptionMaxSeatsMinValue()
    {
        boolean isCorrect = true;

        try{
            new Subscription(UUID.randomUUID(), UUID.randomUUID(), 1, 1, 0, SubscriptionStatus.DONE_STATUS);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }

    @Test()
    public void testSubscriptionMaxSeatsMaxValue()
    {
        boolean isCorrect = true;

        try{
            new Subscription(UUID.randomUUID(), UUID.randomUUID(), 999999, 1, 0, SubscriptionStatus.DONE_STATUS);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }
}
