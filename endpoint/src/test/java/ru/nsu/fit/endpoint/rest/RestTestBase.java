package ru.nsu.fit.endpoint.rest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.glassfish.jersey.internal.util.Base64;
import org.glassfish.jersey.servlet.ServletContainer;
import org.glassfish.jersey.test.DeploymentContext;
import org.glassfish.jersey.test.JerseyTestNg;
import org.glassfish.jersey.test.ServletDeploymentContext;
import org.glassfish.jersey.test.external.ExternalTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import ru.nsu.fit.endpoint.service.database.DB.DBService;
import ru.nsu.fit.endpoint.service.database.DB.ExternalServiceMock;
import ru.nsu.fit.endpoint.service.database.DB.ExternalServiceProvider;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.service.database.enums.UserRole;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;


/*
 TODO
 gson,
 инересные тесты
 мок,
 тестовая xml
 */
public abstract class RestTestBase extends JerseyTestNg.ContainerPerMethodTest {


    protected static final String customerRest = "/rest/customer";
    protected static final String userRest = "/rest/user";
    protected final int SUCCESS = 200;
    protected final int FAILURE = 400;
    protected final Gson gson = new Gson();
    protected final Auth admin = auth("admin", "admin");

    protected AuthData<Customer> customerA;
    protected AuthData<Customer> customerB;

    protected AuthData<User> userA;
    protected AuthData<User> userB;

    protected static String authString(String login, String password) {
        return "Basic " + Base64.encodeAsString(login + ":" + password);
    }

    @BeforeClass
    public final void setup() {
        setupCommonData();
        setupCustomData();
    }


    @Override
    protected Application configure() {
        return new CustomApplication();
    }

    private void setupCommonData() {
        Customer.CustomerData customerData = new Customer.CustomerData("John", "Smmith", "john@smi.th", "password", 0);
        customerA = authData(customerData.getLogin(), customerData.getPass(), DBService.createCustomer(customerData));

        customerData = new Customer.CustomerData("Jane", "Smmith", "jane@smi.th", "password", 0);
        customerB = authData(customerData.getLogin(), customerData.getPass(), DBService.createCustomer(customerData));


        User.UserData userData = new User.UserData("Userone", "User", "user1@user.us", "password", UserRole.USER,
                customerA.getData().getId());
        userA = authData(userData.getLogin(), userData.getPass(), DBService.createUser(userData));


        userData = new User.UserData("Usertwo", "User", "user2@user.us", "password", UserRole.USER,
                customerB.getData().getId());
        userB = authData(userData.getLogin(), userData.getPass(), DBService.createUser(userData));

    }

    @Override
    protected TestContainerFactory getTestContainerFactory() throws TestContainerException {
        return new ExternalTestContainerFactory();
    }

    protected abstract void setupCustomData();

    @AfterClass
    public final void destroy() {
        destroyCustomData();
        destroyCommonData();
    }

    private void destroyCommonData() {
        DBService.deleteUser(userA.getData().getId().toString());
        DBService.deleteUser(userB.getData().getId().toString());
        DBService.deleteCustomer(customerA.getData().getId().toString());
        DBService.deleteCustomer(customerB.getData().getId().toString());
    }

    protected <T> void success(Auth auth, String url, String method, Class<T> resultClass, ResultAction<T> onResult) {
        Response response = request(url, auth).accept(MediaType.APPLICATION_JSON_TYPE).method(method);
        Assert.assertEquals(response.getStatus(), SUCCESS);
        onResult.onResult(
                gson.fromJson(response.readEntity(String.class), resultClass));
    }

    protected void success(Auth auth, String url, String method) {
        Response response = request(url, auth).method(method);
        System.out.println(response.readEntity(String.class));
        Assert.assertEquals(response.getStatus(), SUCCESS);
    }

    protected <T, R> R getData(Auth auth, String url, String method, Class<T> resultClass, ResultTransformation<T, R> onResult){
        Response response = request(url, auth).method(method);
        Assert.assertEquals(response.getStatus(), SUCCESS);
        return onResult.onResult(
                gson.fromJson(response.readEntity(String.class), resultClass));
    }

    protected void failure(Auth auth, String url, String method) {
        Response response = request(url, auth).method(method);
        Assert.assertNotEquals(response.getStatus(), SUCCESS);
    }

    protected void custom(Auth auth, String url, String method, int expectedStatus) {
        Response response = request(url, auth).method(method);
        Assert.assertEquals(response.getStatus(), expectedStatus);
    }

    protected abstract void destroyCustomData();

    protected Auth auth(String login, String password) {
        return new Auth(login, password);
    }

    protected <T> AuthData<T> authData(String login, String password, T data) {
        return new AuthData<>(login, password, data);
    }

    public Invocation.Builder request(final String url, final String login, final String password) {
        return request(url, authString(login, password));
    }

    public Invocation.Builder request(final String url, final Auth auth) {
        return request(url, auth == null ? null : auth.authString);
    }

    public Invocation.Builder request(final String url, final String authString) {
        Invocation.Builder builder = target(url).request();
        if (authString != null) {
            builder = builder.header("Authorization", authString);
        }
        return builder;
    }

    public interface ResultAction<T> {
        void onResult(T result);
    }

    public interface ResultTransformation<T, R> {
        R onResult(T result);
    }

    protected class Auth {
        public final String login;
        public final String password;
        public final String authString;

        public Auth(String login, String password) {
            this.login = login;
            this.password = password;
            authString = authString(login, password);
        }

        public Invocation.Builder request(String url) {
            return RestTestBase.this.request(url, authString);
        }

        @Override
        public String toString() {
            return login;
        }
    }

    protected class AuthData<T> extends Auth {

        private final T data;

        public AuthData(String login, String password, T data) {
            super(login, password);
            this.data = data;
        }

        public T getData() {
            return data;
        }
    }
}
