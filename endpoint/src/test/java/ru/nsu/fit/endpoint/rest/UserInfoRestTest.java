package ru.nsu.fit.endpoint.rest;

import com.google.gson.JsonObject;
import org.testng.annotations.Test;

/**
 * Created on 20.11.2016.
 */
public class UserInfoRestTest extends RestTestBase {
    @Override
    protected void setupCustomData() {

    }

    @Override
    protected void destroyCustomData() {

    }

    @Test(priority = 0)
    public void testGetId(){
        success(userA, userRest+"/"+userA.getData().getData().getLogin()+"/id", "GET" /*,
                JsonObject.class, elem -> customerAId.toString().equals(elem.get("id").getAsString())*/);
    }
/*
    @Test(priority = 0)
    public void testGetIdByAnotherUser(){
        failure(userB, userRest+"/"+userA.getData().getData().getLogin()+"/id", "GET");
    }*/

    @Test(priority = 0)
    public void testGetIdWithoutAuth(){
        failure(null, userRest+"/"+userA.getData().getData().getLogin()+"/id", "GET");
    }

    @Test(priority = 1)
    public void testGetData(){
        success(userA, userRest+"/"+userA.getData().getData().getLogin()+"/data", "GET");
    }

    /*
    @Test(priority = 1)
    public void testGetDataByAnotherUser(){
        failure(userB, userRest+"/"+userA.getData().getData().getLogin()+"/data", "GET");
    }*/

    @Test(priority = 1)
    public void testGetDataWithoutAuth(){
        failure(null, userRest+"/"+userA.getData().getData().getLogin()+"/data", "GET");
    }

    @Test(priority = 1)
    public void testGetRole(){
        success(userA, userRest+"/"+userA.getData().getData().getLogin()+"/role", "GET");
    }

    /*
    @Test(priority = 1)
    public void testGetRoleByAnotherUser(){
        failure(userB, userRest+"/"+userA.getData().getData().getLogin()+"/role", "GET");
    }*/

    @Test(priority = 1)
    public void testGetRoleWithoutAuth(){
        failure(null, userRest+"/"+userA.getData().getData().getLogin()+"/role", "GET");
    }

    @Test(priority = 1)
    public void testGetSubscriptions(){
        success(userA, userRest+"/"+userA.getData().getData().getLogin()+"/subscriptions", "GET");
    }

    /*
    @Test(priority = 1)
    public void testGetSubscriptionsByAnotherUser(){
        failure(userB, userRest+"/"+userA.getData().getData().getLogin()+"/subscriptions", "GET");
    }*/

    @Test(priority = 1)
    public void testGetSubscriptionsWithoutAuth(){
        failure(null, userRest+"/"+userA.getData().getData().getLogin()+"/subscriptions", "GET");
    }

}
