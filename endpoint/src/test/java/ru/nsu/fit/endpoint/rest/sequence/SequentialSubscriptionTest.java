package ru.nsu.fit.endpoint.rest.sequence;

import com.google.gson.JsonObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.rest.RestTestBase;
import ru.nsu.fit.endpoint.service.database.DB.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.yandex.qatools.allure.annotations.Step;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

/**
 * Created on 27.11.2016.
 */
public class SequentialSubscriptionTest extends RestTestBase {


    protected Plan extPlan;
    protected Plan plan;

    @Override
    protected void setupCustomData() {
        Plan.PlanData planData = new Plan.PlanData("plan", "internal plan", 2, 1, 100, 100);
        plan = DBService.createPlan(planData);
        planData = new Plan.PlanData("EXT plan", "external plan", 2, 1, 100, 100);
        extPlan = DBService.createPlan(planData);
    }

    @Override
    protected void destroyCustomData() {

        DBService.deletePlan(plan.getId().toString());
        DBService.deletePlan(extPlan.getId().toString());
    }

    @Step("Getting customer {1} id by {0}")
    public UUID getCustomerId(Auth auth, String customerLogin){
        return getData(auth, customerRest+"/"+customerLogin+"/id", "GET",
                JsonObject.class, elem -> UUID.fromString(elem.get("id").getAsString()));
    }

    @Step("Getting customer {1} money by {0}")
    public int getCustomerMoney(Auth auth, UUID customerId){
        return getCustomerData(auth, customerId).getMoney();
    }

    @Step("Checking customer {1} money equals {2}")
    public void checkEqualsCustomerMoney(Auth auth, UUID customerId, int money){
        Assert.assertEquals(getCustomerData(auth, customerId).getMoney(), money);
    }

    public Customer.CustomerData getCustomerData(Auth auth, UUID customerId){
        return getData(auth, customerRest+"/"+customerId+"/data", "GET", Customer.CustomerData.class, it -> it);
    }

    @Step("Creating customer by {0}, expecting {2}")
    public void createUser(Auth auth, Customer.CustomerData customerData, boolean expecting){
        Response response = request(customerRest+"/create", auth).post(Entity.json(gson.toJson(customerData)));
        if (expecting) {
            Assert.assertEquals(response.getStatus(), SUCCESS);
        } else {
            Assert.assertNotEquals(response.getStatus(), SUCCESS);
        }
    }

    @Step("Filling account of {1} by {2}, expecting {3}")
    public void fillAccount(Auth auth, UUID customerID, int amount, boolean expecting){
        if (expecting){
            success(auth, customerRest+"/"+customerID+"/fillAccount/"+amount, "POST");
        } else {
            failure(auth, customerRest+"/"+customerID+"/fillAccount/"+amount, "POST");
        }
    }

    @Step("Trying to buy plan {1}, expecting {3}")
    public void buyPlan(Auth auth, UUID customerID, UUID planId, boolean expecting){
        if (expecting){
            success(auth, customerRest+"/"+customerID+"/buy_plan/"+planId, "POST");
        } else {
            failure(auth, customerRest+"/"+customerID+"/buy_plan/"+planId, "POST");
        }
    }

    @Step("Getting subscriptions of {1} for plan {2}")
    public UUID getSubscriptions(Auth auth, UUID customerId, UUID planId){
        return getData(auth, customerRest+"/"+customerId+"/subscriptions/"+planId, "GET", String[].class, it -> UUID.fromString(it[0]));
    }

    @Step("Assigning subscription {2} to {1}, expected {3}")
    public void assignSubscription(Auth auth, UUID userId, UUID subscriptionId, boolean expected){
        if (expected){
            success(auth, customerRest+"/user/"+userId+"/subscription/"+subscriptionId, "POST");
        } else {
            failure(auth, customerRest+"/user/"+userId+"/subscription/"+subscriptionId, "POST");
        }
    }

    @Test
    public void testInternalPlan(){
        UUID customerId = customerA.getData().getId();
        Auth customer = customerA;
        buyPlan(customer, customerId, plan.getId(), false);
        fillAccount(customer, customerId, 200, true);
        checkEqualsCustomerMoney(customer, customerId, 200);
        buyPlan(customer, customerId, plan.getId(), true);
        assignSubscription(customer, userA.getData().getId(), getSubscriptions(customer, customerId, plan.getId()), true) ;
        checkEqualsCustomerMoney(customer, customerId, 0);
    }

    @Test
    public void testExternalPlan(){
        UUID customerId = customerB.getData().getId();
        Auth customer = customerB;
        buyPlan(customer, customerId, extPlan.getId(), false);
        fillAccount(customer, customerId, 200, true);
        checkEqualsCustomerMoney(customer, customerId, 200);
        buyPlan(customer, customerId, extPlan.getId(), true);
        assignSubscription(customer, userB.getData().getId(), getSubscriptions(customer, customerId, extPlan.getId()), false) ;
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            return;
        }
        assignSubscription(customer, userB.getData().getId(), getSubscriptions(customer, customerId, extPlan.getId()), true) ;
        checkEqualsCustomerMoney(customer, customerId, 0);
    }
}
