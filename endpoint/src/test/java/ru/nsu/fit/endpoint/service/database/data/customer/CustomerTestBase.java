package ru.nsu.fit.endpoint.service.database.data.customer;

import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 * Created on 09.10.2016.
 */
public abstract class CustomerTestBase {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();


    protected final static String validFirstName = "John";
    protected final static String validLastName = "Smith";
    protected final static String validLogin = "john_smith@gmail.com";
    protected final static String validPassword = "iamerror3";
    protected final static int validMoney = 10;
}
