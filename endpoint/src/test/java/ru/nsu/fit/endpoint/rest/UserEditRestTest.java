package ru.nsu.fit.endpoint.rest;

import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.service.database.enums.UserRole;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import java.net.URLEncoder;
import java.util.UUID;

import static org.testng.Assert.*;

/**
 *
 */
public class UserEditRestTest extends RestTestBase {
    private User.UserData userData;
    private UUID userId;

    @Override
    protected void setupCustomData() {
        userData = new User.UserData("Uuser", "Userrr", "user@user.us", "pass123", UserRole.USER, customerA.getData().getId());
    }

    @Override
    protected void destroyCustomData() {

    }


    /*
    @Test(priority = 0)
    public void testCreateUserWithInvalidCustomerId(){
        Response response = request(customerRest+"/user", customerB).post(Entity.json(gson.toJson(userData)));
        assertNotEquals(response.getStatus(), SUCCESS);
    }*/

    @Test(priority = 0)
    public void testCreateUserByUser(){
        Response response = request(customerRest+"/user", userB).post(Entity.json(gson.toJson(userData)));
        assertNotEquals(response.getStatus(), SUCCESS);
    }

    @Test(priority = 0)
    public void testCreateUserWithoutAuth(){
        Response response = target(customerRest).request().post(Entity.json(gson.toJson(userData)));
        assertNotEquals(response.getStatus(), SUCCESS);
    }

    @Test(priority = 1)
    public void testCreateUser(){
        Response response = request(customerRest+"/user", customerA).post(Entity.json(gson.toJson(userData)));
        assertEquals(response.getStatus(), SUCCESS);
        // TODO assign userId
    }

    /*
    @Test(priority = 2)
    public void testCreateUserAgain(){
        Response response = request(customerRest+"/user", customerA).post(Entity.json(gson.toJson(userData)));
        assertNotEquals(response.getStatus(), SUCCESS);
    }

    @Test(priority = 3)
    public void testCreateUserAgainByAnotherCustomer(){
        Response response = request(customerRest+"/user", customerB).post(Entity.json(gson.toJson(userData)));
        assertNotEquals(response.getStatus(), SUCCESS);
    }

    @Test(priority = 4)
    public void testAssignRole(){
        success(customerA, customerRest+"/user/"+userId+"/role/Billing administrator", "POST");
    }*/

    @Test(priority = 4)
    public void testAssignRoleByOtherCustomer(){
        failure(customerB, customerRest+"/user/"+userId+"/role/Technical administrator", "POST");
    }

    @Test(priority = 4)
    public void testAssignRoleByUser(){
        failure(userA, customerRest+"/user/"+userId+"/role/Technical administrator", "POST");
    }

    @Test(priority = 4)
    public void testAssignRoleWithoutAuth(){
        failure(null, customerRest+"/user/"+userId+"/role/Technical administrator", "POST");
    }

    /*
    @Test(priority = 5)
    public void testDeleteUserByAnotherCustomer(){
        failure(customerB, customerRest+"/user/"+userId, "DELETE");
    }*/

    @Test(priority = 5)
    public void testDeleteUserByUser(){
        failure(userB, customerRest+"/user/"+userId, "DELETE");
    }

    @Test(priority = 5)
    public void testDeleteUserWithoutAuth(){
        failure(null, customerRest+"/user/"+userId, "DELETE");
    }

    @Test(priority = 6)
    public void testDeleteUser(){
        success(customerA, customerRest+"/user/"+userId, "DELETE");
    }
}
