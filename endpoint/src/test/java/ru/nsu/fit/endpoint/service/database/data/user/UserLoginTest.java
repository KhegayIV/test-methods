package ru.nsu.fit.endpoint.service.database.data.user;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.exceptions.User.UserNameException;
import ru.nsu.fit.endpoint.service.database.exceptions.User.UserNullFieldException;
import ru.nsu.fit.endpoint.service.database.data.User;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class UserLoginTest extends UserTestBase{

    @Test
    public void testNull() {
        expectedEx.expect(UserNullFieldException.class);
        expectedEx.expectMessage("Login should be specified");
        new User(validFirstName, validLastName, null, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testFirstLvDomain() {
        expectedEx.expect(UserNameException.class);
        expectedEx.expectMessage("Login should be an email");
        new User(validFirstName, validLastName, "johnsmith@com", validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testThirdLvDomain() {
        new User(validFirstName, validLastName, "johnsmith@site.corporation.com", validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testDot() {
        new User(validFirstName, validLastName, "john.smith@gmail.com", validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testDoubleAt() {
        expectedEx.expect(UserNameException.class);
        expectedEx.expectMessage("Login should be an email");
        new User(validFirstName, validLastName, "john.smith@gmail@com", validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }


    @Test
    public void testInvalidSymbols() {
        expectedEx.expect(UserNameException.class);
        expectedEx.expectMessage("Login should be an email");
        new User(validFirstName, validLastName, "john#$#$#smith@gmail.com", validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }


}
