package ru.nsu.fit.endpoint.service.database.data.serviceplan;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.exceptions.IllegalSeatsException;
import ru.nsu.fit.endpoint.service.database.exceptions.MaxSeatsRangeException;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanException;

import static org.junit.Assert.assertTrue;

public class ServicePlanMaxSeatsTests
{
    @Test(expected = MaxSeatsRangeException.class)
    public void testServicePlanTooLittleMaxSeats()
    {
        new Plan("name", "details", 0, 1, 1, 1);
    }

    @Test(expected = MaxSeatsRangeException.class)
    public void testServicePlanTooBigMaxSeats()
    {
        new Plan("name", "details", 1000000, 1, 1, 1);
    }

    @Test(expected = IllegalSeatsException.class)
    public void testServicePlanMaxSeatsSmallerThenMinSeats()
    {
        new Plan("name", "details", 1, 2, 1, 1);
    }

    @Test()
    public void testServicePlanMaxSeatsMinValue()
    {
        boolean isCorrect = true;

        try{
            new Plan("name", "details", 1, 1, 0, 1);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }

    @Test()
    public void testServicePlanMaxSeatsMaxValue()
    {
        boolean isCorrect = true;

        try{
            new Plan("name", "details", 999999, 1, 0, 1);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }
}
