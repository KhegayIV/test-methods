package ru.nsu.fit.endpoint.service.database.data.customer;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.exceptions.Customer.CustomerNegativeMoneyException;
import ru.nsu.fit.endpoint.service.database.data.Customer;

/**
 * Created on 09.10.2016.
 */
public class CustomerMoneyTest extends CustomerTestBase{
    @Test
    public void testValid() {
        new Customer(validFirstName, validLastName, validLogin, validPassword, 0);
    }

    @Test
    public void testInvalid() {
        expectedEx.expect(CustomerNegativeMoneyException.class);
        new Customer(validFirstName, validLastName, validLogin, validPassword, -1);
    }
}
