package ru.nsu.fit.endpoint.service.database.data.serviceplan;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanException;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanNameLengthException;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanNameWrongContentException;

import static org.junit.Assert.assertTrue;

public class ServicePlanNameTests
{
    @Test(expected = ServicePlanNameLengthException.class)
    public void testServicePlanShortName()
    {
        String name = " ";
        new Plan(name, "details", 2, 1, 1, 1);
    }

    @Test()
    public void testServicePlanNameMinCorrectLength()
    {
        boolean isCorrect = true;
        String name = "na";
        try
        {
            new Plan(name, "details", 2, 1, 1, 1);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }

    @Test()
    public void testServicePlanNameMaxCorrectLength()
    {
        boolean isCorrect = true;
        String name = "This is a long long long long long long long long long long long long long long long long long long long long long long stringgg";
        try
        {
            new Plan(name, "details", 2, 1, 1, 1);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }

    @Test(expected = ServicePlanNameLengthException.class)
    public void testServicePlanLongName()
    {
        String name = "This is a лонг long long long long long long long long long long long long long long long long long long long long long stringggg";
        new Plan(name, "details", 2, 1, 1, 1);
    }

    @Test(expected = ServicePlanNameWrongContentException.class)
    public void testServicePlanWrongContentName()
    {
        String name = "&*/";
        new Plan(name, "details", 2, 1, 1, 1);
    }
}
