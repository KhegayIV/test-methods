package ru.nsu.fit.endpoint.service.database.data.customer;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.data.Customer;

/**
 * Created on 09.10.2016.
 */
public class CustomerTestValid extends CustomerTestBase {
    @Test
    public void testValid() {
        new Customer(validFirstName, validLastName, validLogin, validPassword, validMoney);
    }
}
