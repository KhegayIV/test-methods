package ru.nsu.fit.endpoint.service.database.data.subscriptions;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.enums.SubscriptionStatus;
import ru.nsu.fit.endpoint.service.database.exceptions.IllegalSeatsException;
import ru.nsu.fit.endpoint.service.database.exceptions.MinSeatsRangeException;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanException;

import java.util.UUID;

import static org.junit.Assert.assertTrue;

public class SubscriptionMinSeatsTests {
    @Test(expected = MinSeatsRangeException.class)
    public void testSubscriptionTooLittleMinSeats() {
        new Subscription(UUID.randomUUID(), UUID.randomUUID(), 1, 0, 0, SubscriptionStatus.DONE_STATUS);
    }

    @Test(expected = MinSeatsRangeException.class)
    public void testSubscriptionTooBigMinSeats() {
        new Subscription(UUID.randomUUID(), UUID.randomUUID(), 2, 1000000, 0, SubscriptionStatus.DONE_STATUS);
    }

    @Test(expected = IllegalSeatsException.class)
    public void testSubscriptionMaxSeatsSmallerThenMinSeats() {
        new Subscription(UUID.randomUUID(), UUID.randomUUID(), 1, 2, 0, SubscriptionStatus.DONE_STATUS);
    }

    @Test()
    public void testSubscriptionMinSeatsMinValue() {
        boolean isCorrect = true;

        try {
            new Subscription(UUID.randomUUID(), UUID.randomUUID(), 1, 1, 0, SubscriptionStatus.DONE_STATUS);
        } catch (ServicePlanException e) {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }

    @Test()
    public void testSubscriptionMinSeatsMaxValue() {
        boolean isCorrect = true;

        try {
            new Subscription(UUID.randomUUID(), UUID.randomUUID(), 999999, 999999, 0, SubscriptionStatus.DONE_STATUS);
        } catch (ServicePlanException e) {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }
}
