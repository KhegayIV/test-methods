package ru.nsu.fit.endpoint.rest;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.rest.responses.CustomerDataResponse;

/**
 *
 */
public class CustomerInfoRestTest extends RestTestBase {
    @Override
    protected void setupCustomData() {

    }

    @Override
    protected void destroyCustomData() {

    }

    @Test(priority = 0)
    public void testGetId(){
        success(customerA, customerRest+"/"+customerA.getData().getData().getLogin()+"/id", "GET",
                JsonObject.class, elem -> customerA.getData().getId().toString().equals(elem.get("id").getAsString()));
    }

    /*
    @Test(priority = 0)
    public void testGetIdByOtherCustomer(){
        failure(customerB, customerRest+"/"+customerA.getData().getData().getLogin()+"/id", "GET");
    }*/

    @Test(priority = 0)
    public void testGetIdWithoutAuth(){
        failure(null, customerRest+"/"+customerA.getData().getData().getLogin()+"/id", "GET");
    }

    @Test(priority = 1)
    public void testGetData(){
        success(customerA, customerRest+"/"+customerA.getData().getId()+"/data", "GET");
    }

    /*
    @Test(priority = 1)
    public void testGetDataByOtherCustomer(){
        failure(customerA, customerRest+"/"+customerA.getData().getId()+"/data", "GET");
    }*/

    @Test(priority = 1)
    public void testGetDataWithoutAuth(){
        failure(null, customerRest+"/"+customerA.getData().getId()+"/data", "GET");
    }

    @Test(priority = 1)
    public void testGetUsers(){
        success(customerA, customerRest+"/"+customerA.getData().getId()+"/users", "GET");
    }

    /*
    @Test(priority = 1)
    public void testGetUsersByOtherCustomer(){
        failure(customerB, customerRest+"/"+customerA.getData().getId()+"/users", "GET");
    }*/

    @Test(priority = 1)
    public void testGetUsersWithoutAuth(){
        failure(null, customerRest+"/"+customerA.getData().getId()+"/users", "GET");
    }

    @Test(priority = 1)
    public void testGetPlans(){
        success(customerA, customerRest+"/"+customerA.getData().getId()+"/plans", "GET");
    }

    /*
    @Test(priority = 1)
    public void testGetPlansByOtherCustomer(){
        failure(customerB, customerRest+"/"+customerA.getData().getId()+"/plans", "GET");
    }*/

    @Test(priority = 1)
    public void testGetPlansWithoutAuth(){
        failure(null, customerRest+"/"+customerA.getData().getId()+"/plans", "GET");
    }

    @Test(priority = 1)
    public void testGetSubscriptions(){
        success(customerA, customerRest+"/"+customerA.getData().getId()+"/subscriptions", "GET");
    }

    /*
    @Test(priority = 1)
    public void testGetSubscriptionsByOtherCustomer(){
        failure(customerA, customerRest+"/"+customerA.getData().getId()+"/subscriptions", "GET");
    }*/

    @Test(priority = 1)
    public void testGetSubscriptionsWithoutAuth(){
        failure(null, customerRest+"/"+customerA.getData().getId()+"/subscriptions", "GET");
    }
}
