package ru.nsu.fit.endpoint.service.database.data.customer;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.exceptions.Customer.CustomerNameException;
import ru.nsu.fit.endpoint.service.database.exceptions.Customer.CustomerNullFieldException;
import ru.nsu.fit.endpoint.service.database.data.Customer;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CustomerFirstNameTest extends CustomerTestBase{

    @Test
    public void testNull() {
        expectedEx.expect(CustomerNullFieldException.class);
        expectedEx.expectMessage("First name should be specified");
        new Customer(null, validLastName, validLogin, validPassword, validMoney);
    }

    @Test
    public void testShort() {
        expectedEx.expect(CustomerNameException.class);
        expectedEx.expectMessage("First name's length should be 2 symbols or more");
        new Customer("J", validLastName, validLogin, validPassword, validMoney);
    }

    @Test
    public void testShortEnough() {
        new Customer("Jo", validLastName, validLogin, validPassword, validMoney);
    }

    @Test
    public void testLong() {
        expectedEx.expect(CustomerNameException.class);
        expectedEx.expectMessage("First name's length should be 12 symbols or less");
        new Customer("Johnjamesreginaldmarcussmith", validLastName, validLogin, validPassword, validMoney);
    }

    @Test
    public void testLongEnough() {
        new Customer("Johnathanado", validLastName, validLogin, validPassword, validMoney);
    }

    @Test
    public void testSmallLetter(){
        expectedEx.expect(CustomerNameException.class);
        expectedEx.expectMessage("First name has invalid format");
        new Customer("john", validLastName, validLogin, validPassword, validMoney);
    }

    @Test
    public void testBigLetter(){
        expectedEx.expect(CustomerNameException.class);
        expectedEx.expectMessage("First name has invalid format");
        new Customer("JohN", validLastName, validLogin, validPassword, validMoney);
    }

    @Test
    public void testIllegalSymbols(){
        expectedEx.expect(CustomerNameException.class);
        expectedEx.expectMessage("First name has invalid format");
        new Customer("John1", validLastName, validLogin, validPassword, validMoney);
    }

}
