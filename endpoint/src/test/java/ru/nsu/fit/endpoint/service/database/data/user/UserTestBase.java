package ru.nsu.fit.endpoint.service.database.data.user;

import org.junit.Rule;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.service.database.enums.UserRole;

import java.util.UUID;

/**
 * Created on 09.10.2016.
 */
public abstract class UserTestBase {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();


    protected final static String validFirstName = "John";
    protected final static String validLastName = "Smith";
    protected final static String validLogin = "john_smith@gmail.com";
    protected final static String validPassword = "iamerror3";
    protected final static UserRole validUserRole = UserRole.USER;
    protected final static UUID validCustomerId = UUID.randomUUID();
    protected final static UUID[] validSubscriptionIds = new UUID[]{UUID.randomUUID()};

}
