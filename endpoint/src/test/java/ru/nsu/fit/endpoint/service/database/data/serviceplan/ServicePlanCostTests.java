package ru.nsu.fit.endpoint.service.database.data.serviceplan;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanCostRangeException;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanException;

import static org.junit.Assert.assertTrue;

/**
 * Created by Tarri on 12.11.2016.
 */
public class ServicePlanCostTests {
    @Test(expected = ServicePlanCostRangeException.class)
    public void testServicePlanTooLittleCost()
    {
        new Plan("name", "details", 2, 1, 0, -1);
    }

    @Test(expected = ServicePlanCostRangeException.class)
    public void testServicePlanTooBigCost()
    {
        new Plan("name", "details", 2, 1, 0, 1000000);
    }

    @Test()
    public void testServicePlanCostMinValue()
    {
        boolean isCorrect = true;

        try{
            new Plan("name", "details", 2, 1, 1, 0);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }

    @Test()
    public void testServicePlanCostMaxValue()
    {
        boolean isCorrect = true;

        try{
            new Plan("name", "details", 2, 1, 1, 999999);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }

}
