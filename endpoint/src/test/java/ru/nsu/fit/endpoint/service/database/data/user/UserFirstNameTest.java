package ru.nsu.fit.endpoint.service.database.data.user;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.exceptions.User.UserNameException;
import ru.nsu.fit.endpoint.service.database.exceptions.User.UserNullFieldException;
import ru.nsu.fit.endpoint.service.database.data.User;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class UserFirstNameTest extends UserTestBase{

    @Test
    public void testNull() {
        expectedEx.expect(UserNullFieldException.class);
        expectedEx.expectMessage("First name should be specified");
        new User(null, validLastName, validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testShort() {
        expectedEx.expect(UserNameException.class);
        expectedEx.expectMessage("First name's length should be 2 symbols or more");
        new User("J", validLastName, validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testShortEnough() {
        new User("Jo", validLastName, validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testLong() {
        expectedEx.expect(UserNameException.class);
        expectedEx.expectMessage("First name's length should be 12 symbols or less");
        new User("Johnjamesreginaldmarcussmith", validLastName, validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testLongEnough() {
        new User("Johnathanado", validLastName, validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testSmallLetter(){
        expectedEx.expect(UserNameException.class);
        expectedEx.expectMessage("First name has invalid format");
        new User("john", validLastName, validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testBigLetter(){
        expectedEx.expect(UserNameException.class);
        expectedEx.expectMessage("First name has invalid format");
        new User("JohN", validLastName, validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

    @Test
    public void testIllegalSymbols(){
        expectedEx.expect(UserNameException.class);
        expectedEx.expectMessage("First name has invalid format");
        new User("John1", validLastName, validLogin, validPassword, validUserRole, validCustomerId, validSubscriptionIds);
    }

}
