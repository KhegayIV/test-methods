package ru.nsu.fit.endpoint.rest.sequence;

import com.google.gson.JsonObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.rest.RestTestBase;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.service.database.enums.UserRole;
import ru.yandex.qatools.allure.annotations.Step;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.UUID;

/**
 * Created on 27.11.2016.
 */
public class SequentialUserEditRestTest extends RestTestBase {

    @Step("Creating user by {0}, expecting {2}")
    public void createUser(Auth auth, User.UserData userData, boolean expecting){
        Response response = request(customerRest+"/user", auth).post(Entity.json(gson.toJson(userData)));
        if (expecting) {
            Assert.assertEquals(response.getStatus(), SUCCESS);
        } else {
            Assert.assertNotEquals(response.getStatus(), SUCCESS);
        }
    }


    @Step("Assigning user {1} role {2} by {0}, expecting {3}")
    public void assignRole(Auth auth, String userId, String role, boolean expecting){
        if (expecting) {
            success(auth, customerRest +"/user/" + userId + "/role/" + role, "POST");
        } else {
            failure(auth, customerRest +"/user/" + userId + "/role/" + role, "POST");
        }
    }

    @Step("Deleting user {1} by {0}, expecting {3}")
    public void deleteUser(Auth auth, String userId, boolean expecting){
        if (expecting) {
            success(auth, customerRest +"/user/" + userId, "DELETE");
        } else {
            failure(auth, customerRest +"/user/" + userId, "DELETE");
        }
    }

    @Step("Getting user {1} id by {0}")
    public UUID getUserId(Auth auth, String userLogin){
        return getData(auth, userRest+"/"+userLogin+"/id", "GET",
                    JsonObject.class, elem -> UUID.fromString(elem.get("id").getAsString()));
    }

    @Test
    public void testUserManipulation(){
        User.UserData userData =
                new User.UserData("Userthree", "Userthree", "user3@user.us", "123456", UserRole.USER, customerA.getData().getId());
        Auth userAuth = auth(userData.getLogin(), userData.getPass());
        createUser(customerA, userData, true);
        UUID userId = getUserId(userAuth, userData.getLogin());
        assignRole(null, userId.toString(), UserRole.BILLING_ADMINISTRATOR.toString(), false);
        assignRole(userAuth, userId.toString(), UserRole.COMPANY_ADMINISTRATOR.toString(), false);
        assignRole(customerA, userId.toString(), UserRole.TECHNICAL_ADMINISTRATOR.toString(), true);
        deleteUser(null, userId.toString(), false);
        deleteUser(userAuth, userId.toString(), false);
        deleteUser(customerA, userId.toString(), true);
    }

    @Override
    protected void setupCustomData() {

    }

    @Override
    protected void destroyCustomData() {

    }
}
