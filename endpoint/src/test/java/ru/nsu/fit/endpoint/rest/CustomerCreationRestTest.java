package ru.nsu.fit.endpoint.rest;

import static org.testng.Assert.*;

import org.testng.annotations.Test;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.service.database.enums.UserRole;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * TODO: Требуются методы для проверки наличия кастомера, для удаления кастомера
 */

public class CustomerCreationRestTest extends RestTestBase {
    private Customer.CustomerData customerData;


    @Override
    protected void setupCustomData() {
        customerData = new Customer.CustomerData("Ivan", "Ivanko", "Ivan@ivan.iv", "passw123", 1000);
    }

    @Override
    protected void destroyCustomData() {

    }

    @Test
    public void testCreateCustomerWithoutAuth(){
        Response response = target(customerRest+"/create").request().post(Entity.json(gson.toJson(customerData)));
        assertNotEquals(response.getStatus(), SUCCESS);
    }

    @Test
    public void testCreateCustomerByCustomer(){
        Response response = request(customerRest+"/create", customerA).post(Entity.json(gson.toJson(customerData)));
        assertNotEquals(response.getStatus(), SUCCESS);
    }

    @Test
    public void testCreateCustomerByUser(){
        Response response = request(customerRest+"/create", userA).post(Entity.json(gson.toJson(customerData)));
        assertNotEquals(response.getStatus(), SUCCESS);
    }

    @Test(priority = 1)
    public void testCreateCustomer(){
        Response response = admin.request(customerRest+"/create").post(Entity.json(gson.toJson(customerData)));
        assertEquals(response.getStatus(), SUCCESS);
    }

    /*
    @Test(priority = 2)
    public void testCreateCustomerAgain(){
        Response response = admin.request(customerRest+"/create").post(Entity.json(gson.toJson(customerData)));
        assertNotEquals(response.getStatus(), SUCCESS);
    }*/


}
