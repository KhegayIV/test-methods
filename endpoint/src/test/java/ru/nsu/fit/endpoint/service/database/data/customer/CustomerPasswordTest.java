package ru.nsu.fit.endpoint.service.database.data.customer;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.exceptions.Customer.CustomerInvalidPassException;
import ru.nsu.fit.endpoint.service.database.exceptions.Customer.CustomerNullFieldException;
import ru.nsu.fit.endpoint.service.database.data.Customer;

/**
 * Created on 09.10.2016.
 */
public class CustomerPasswordTest extends CustomerTestBase{

    @Test
    public void testNull() {
        expectedEx.expect(CustomerNullFieldException.class);
        expectedEx.expectMessage("Password should be specified");
        new Customer(validFirstName, validLastName, validLogin, null, validMoney);
    }

    @Test
    public void testShort() {
        expectedEx.expect(CustomerInvalidPassException.class);
        expectedEx.expectMessage("Password's length should be 6 symbols or more");
        new Customer(validFirstName, validLastName, validLogin, "pwd", validMoney);
    }

    @Test
    public void testShortEnough() {
        new Customer(validFirstName, validLastName, validLogin, "paswrd", validMoney);
    }

    @Test
    public void testLong() {
        expectedEx.expect(CustomerInvalidPassException.class);
        expectedEx.expectMessage("Password's length should be 12 symbols or less");
        new Customer(validFirstName, validLastName, validLogin, "thisisaveryveryverylongpassword", validMoney);
    }

    @Test
    public void testLongEnough() {
        new Customer(validFirstName, validLastName, validLogin, "barelyenough", validMoney);
    }

    @Test
    public void testEasyPass(){
        expectedEx.expect(CustomerInvalidPassException.class);
        expectedEx.expectMessage("Password is too easy");
        new Customer(validFirstName, validLastName, validLogin, "qwerty11123", validMoney);
    }

    @Test
    public void testFirstName(){
        expectedEx.expect(CustomerInvalidPassException.class);
        expectedEx.expectMessage("First name can't be part of a password");
        new Customer("John", validLastName, validLogin, "John'sPass", validMoney);
    }

    @Test
    public void testLastName(){
        expectedEx.expect(CustomerInvalidPassException.class);
        expectedEx.expectMessage("Last name can't be part of a password");
        new Customer(validFirstName, "Smith", validLogin, "IAmSmith", validMoney);
    }

    @Test
    public void testLogin(){
        expectedEx.expect(CustomerInvalidPassException.class);
        expectedEx.expectMessage("Login can't be part of a password");
        new Customer(validFirstName, validLastName, "a@b.cd", "a@b.cd1234", validMoney);
    }
}
