package ru.nsu.fit.endpoint.service.database.data.serviceplan;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanDetailsLengthException;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanException;

import static org.junit.Assert.assertTrue;

public class ServicePlanDetailsTests
{
    @Test(expected = ServicePlanDetailsLengthException.class)
    public void testServicePlanShortDetails()
    {
        String details = "";
        new Plan("name", details, 2, 1, 1, 1);
    }

    @Test(expected = ServicePlanDetailsLengthException.class)
    public void testServicePlanLongDetails()
    {
        String details = " details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details ";
        new Plan("name", details, 2, 1, 1, 1);
    }

    @Test()
    public void testServicePlanDetailsMinLength()
    {
        boolean isCorrect = true;

        String details = "d";
        try{
            new Plan("name", details, 2, 1, 1, 1);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }

    @Test()
    public void testServicePlanDetailsMaxLength()
    {
        boolean isCorrect = true;

        String details = " details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details details";
        try
        {
            new Plan("name", details, 2, 1, 1, 1);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }
}
