package ru.nsu.fit.endpoint.service.database.data.serviceplan;

import org.junit.Test;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.exceptions.IllegalSeatsException;
import ru.nsu.fit.endpoint.service.database.exceptions.MinSeatsRangeException;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.ServicePlanException;

import static org.junit.Assert.assertTrue;

public class ServicePlanMinSeatsTests
{
    @Test(expected = MinSeatsRangeException.class)
    public void testServicePlanTooLittleMinSeats()
    {
        new Plan("name", "details", 2, 0, 1, 1);
    }

    @Test(expected = MinSeatsRangeException.class)
    public void testServicePlanTooBigMinSeats()
    {
        new Plan("name", "details", 2, 1000000, 1, 1);
    }

    @Test(expected = IllegalSeatsException.class)
    public void testServicePlanMaxSeatsSmallerThenMinSeats()
    {
        new Plan("name", "details", 1, 2, 1, 1);
    }

    @Test()
    public void testServicePlanMinSeatsMinValue()
    {
        boolean isCorrect = true;

        try{
            new Plan("name", "details", 1, 1, 0, 1);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }

    @Test()
    public void testServicePlanMinSeatsMaxValue()
    {
        boolean isCorrect = true;

        try{
            new Plan("name", "details", 999999, 999999, 0, 1);
        }
        catch (ServicePlanException e)
        {
            isCorrect = false;
        }
        assertTrue(isCorrect);
    }
}
