package ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan;

import ru.nsu.fit.endpoint.service.database.exceptions.BaseException;

public class ServicePlanException extends BaseException
{
    protected ServicePlanException(String mess)
    {
        super(mess);
    }
}
