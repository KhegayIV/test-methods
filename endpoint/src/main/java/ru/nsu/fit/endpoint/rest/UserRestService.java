package ru.nsu.fit.endpoint.rest;

import org.apache.commons.lang.exception.ExceptionUtils;
import ru.nsu.fit.endpoint.rest.responses.SubscriptionsResponse;
import ru.nsu.fit.endpoint.rest.responses.UsersResponse;
import ru.nsu.fit.endpoint.service.database.DB.DBService;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.service.database.enums.UserRole;

import javax.annotation.security.RolesAllowed;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Path("/rest/user")
@Singleton
public class UserRestService {
    @Context SecurityContext sc;

    @RolesAllowed("USER")
    @GET
    @Path("/{user_login}/id")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response getUserId(@PathParam("user_login") String userLogin) {
        try {
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, userLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();

            UUID id = DBService.getUserIdByLogin(userLogin);
            return Response.status(200).entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("USER")
    @GET
    @Path("/{user_login}/role")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response getUserRole(@PathParam("user_login") String userLogin) {
        try {
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, userLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();

            UserRole role = DBService.getUserRoleByLogin(userLogin);
            return Response.status(200).entity("{\"role\":\"" + role.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("USER")
    @GET
    @Path("/{user_login}/data")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response getUserData(@PathParam("user_login") String userLogin) {
        try {
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, userLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();

            UUID id = DBService.getUserIdByLogin(userLogin);
            User user = DBService.getUser(id.toString());

            ArrayList<User> users = new ArrayList<>();
            users.add(user);
            UsersResponse response = new UsersResponse(users);
            return Response.status(200).entity(response.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("USER")
    @GET
    @Path("/{user_login}/subscriptions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response getUserSubscriptions(@PathParam("user_login") String userLogin) {
        try {
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, userLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();

            List<Subscription> subscriptions = DBService.getUserSubscriptionsByLogin(userLogin);
            SubscriptionsResponse response = new SubscriptionsResponse(subscriptions);
            return Response.status(200).entity(response.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

}
