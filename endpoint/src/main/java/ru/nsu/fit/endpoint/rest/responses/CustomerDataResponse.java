package ru.nsu.fit.endpoint.rest.responses;

import ru.nsu.fit.endpoint.service.database.data.Customer;

public class CustomerDataResponse {
    private Customer.CustomerData customerData = null;

    public CustomerDataResponse(Customer.CustomerData customerData) {
        this.customerData = customerData;
    }

    @Override
    public String toString(){
            return "{\"firstName\":\"" + customerData.getFirstName() + "\",\n" +
                    "\"lastName\":\"" + customerData.getLastName() + "\",\n" +
                    "\"login\":\"" + customerData.getLogin() + "\",\n" +
                    "\"password\":\"" + customerData.getPass() + "\",\n" +
                    "\"money\":\"" + customerData.getMoney() + "\",\n" +
                    "}";
    }
}
