package ru.nsu.fit.endpoint.service.database.exceptions.User;

/**
 * Created on 09.10.2016.
 */
public class UserNullFieldException extends UserDataException {
    public UserNullFieldException(String fieldName) {
        super(fieldName + " should be specified");
    }
}
