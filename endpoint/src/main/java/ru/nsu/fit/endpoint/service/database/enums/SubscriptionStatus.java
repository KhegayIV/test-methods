package ru.nsu.fit.endpoint.service.database.enums;

public enum SubscriptionStatus {
    PROVISIONING_STATUS("Provisioning"),
    DONE_STATUS("Done");
    private String status;

    SubscriptionStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
