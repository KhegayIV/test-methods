package ru.nsu.fit.endpoint.service.database.exceptions.Customer;

/**
 * Created on 09.10.2016.
 */
public class CustomerDataException extends IllegalArgumentException {
    public CustomerDataException() {
    }

    public CustomerDataException(String s) {
        super(s);
    }

    public CustomerDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public CustomerDataException(Throwable cause) {
        super(cause);
    }
}
