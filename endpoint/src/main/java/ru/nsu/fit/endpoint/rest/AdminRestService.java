package ru.nsu.fit.endpoint.rest;

import org.apache.commons.lang.exception.ExceptionUtils;
import ru.nsu.fit.endpoint.rest.responses.CustomersResponse;
import ru.nsu.fit.endpoint.service.database.DB.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;

@Path("/rest")
@Singleton
public class AdminRestService {
    @Context
    SecurityContext sc;

    @PermitAll
    @GET
    @Path("/get_role")
    @Produces(MediaType.APPLICATION_JSON)
    public static Response getRole(){
//        try {
//            List<Customer> customers = DBService.getAllCustomers();
//            CustomersResponse response = new CustomersResponse(customers);
//            return Response.status(200).entity(response.toString()).build();
//        } catch (IllegalArgumentException ex) {
//            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
//        }
        return null;
    }

    @RolesAllowed("ADMIN")
    @POST
    @Path("/customer/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Response createCustomer(String customerDataJson) {
        try {
            Customer.CustomerData customerData = JsonMapper.fromJson(customerDataJson, Customer.CustomerData.class);
            DBService.createCustomer(customerData);
            return Response.status(200).entity(customerData.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("ADMIN")
    @GET
    @Path("/customer/all")
    @Produces(MediaType.APPLICATION_JSON)
    public static Response getCustomers() {
        try {

            List<Customer> customers = DBService.getAllCustomers();
            CustomersResponse response = new CustomersResponse(customers);
            return Response.status(200).entity(response.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
}
