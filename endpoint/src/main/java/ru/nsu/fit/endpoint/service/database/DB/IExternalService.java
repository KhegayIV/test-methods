package ru.nsu.fit.endpoint.service.database.DB;

import ru.nsu.fit.endpoint.service.database.data.Plan;

public interface IExternalService {
    public boolean IsExternal(String planName);
    public void BuyExternalPlan(String customerId, Plan plan);
}
