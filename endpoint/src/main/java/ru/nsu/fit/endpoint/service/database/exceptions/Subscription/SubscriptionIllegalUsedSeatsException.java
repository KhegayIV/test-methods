package ru.nsu.fit.endpoint.service.database.exceptions.Subscription;

public class SubscriptionIllegalUsedSeatsException extends SubscriptionException
{
    public SubscriptionIllegalUsedSeatsException()
    {
        super("Used seats should be smaller or equal then max seats");
    }
}
