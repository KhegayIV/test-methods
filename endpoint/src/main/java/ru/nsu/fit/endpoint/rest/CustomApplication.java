package ru.nsu.fit.endpoint.rest;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import ru.nsu.fit.endpoint.service.database.DB.ExternalServiceMock;
import ru.nsu.fit.endpoint.service.database.DB.ExternalServiceProvider;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CustomApplication extends ResourceConfig {
    public CustomApplication() {
        super();
        ExternalServiceProvider.setService(new ExternalServiceMock());
        packages("ru.nsu.fit.endpoint.rest");
        register(RolesAllowedDynamicFeature.class);
        register(LoggingFilter.class);
        register(AuthenticationFilter.class);
        register(GsonMessageBodyHandler.class);
    }
}
