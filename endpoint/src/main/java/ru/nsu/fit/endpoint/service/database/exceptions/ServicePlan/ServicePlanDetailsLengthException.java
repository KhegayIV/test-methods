package ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan;

public class ServicePlanDetailsLengthException extends ServicePlanException
{
    public  ServicePlanDetailsLengthException()
    {
        super("Details length should be more or equal 1 symbols and less or equal 1024 symbols");
    }
}
