package ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan;

public class ServicePlanFeePerUnitRangeException extends ServicePlanException
{
    public ServicePlanFeePerUnitRangeException()
    {
        super("Fee per unit should be between 0 and 999999");
    }
}
