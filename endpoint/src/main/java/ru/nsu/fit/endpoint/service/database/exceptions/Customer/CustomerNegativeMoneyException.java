package ru.nsu.fit.endpoint.service.database.exceptions.Customer;

/**
 * Created on 09.10.2016.
 */
public class CustomerNegativeMoneyException extends CustomerDataException {
    public CustomerNegativeMoneyException() {
        super("Money can't be less than zero");
    }

    public CustomerNegativeMoneyException(Throwable cause) {
        super("Money can't be less than zero", cause);
    }
}
