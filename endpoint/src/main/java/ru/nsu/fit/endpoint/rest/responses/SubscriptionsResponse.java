package ru.nsu.fit.endpoint.rest.responses;

import ru.nsu.fit.endpoint.service.database.data.Subscription;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionsResponse {
    private List<Subscription> subscriptions = null;

    public  SubscriptionsResponse(List<Subscription> subscriptions){
        this.subscriptions = new ArrayList<>(subscriptions);
    }

    @Override
    public String toString(){
        String res = "{\n";
        for(int i = 0 ; i < subscriptions.size() ; i++){
            res += "\"subscription\":{\n" +
                    "\"id\":\"" + subscriptions.get(i).getId().toString() + "\",\n" +
                    "\"planId\":\"" + subscriptions.get(i).getData().getServicePlanId().toString() + "\",\n" +
                    "\"status\":\"" + subscriptions.get(i).getData().getStatus() + "\",\n" +
                    "\"maxSeats\":\"" + subscriptions.get(i).getData().getMaxSeats() + "\",\n" +
                    "\"minSeats\":\"" + subscriptions.get(i).getData().getMinSeats() + "\",\n" +
                    "\"usedSeats\":\"" + subscriptions.get(i).getData().getUsedSeats() + "\"\n" +
                    "},";
        }

        //TODO удалить последнюю запятую
        res += "\n}";
        return res;
    }
}
