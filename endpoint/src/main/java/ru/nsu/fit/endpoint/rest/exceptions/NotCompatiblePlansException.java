package ru.nsu.fit.endpoint.rest.exceptions;

import ru.nsu.fit.endpoint.service.database.exceptions.BaseException;

public class NotCompatiblePlansException extends BaseException {
    public NotCompatiblePlansException() {
        super("Selected plans are not compatible");
    }
}