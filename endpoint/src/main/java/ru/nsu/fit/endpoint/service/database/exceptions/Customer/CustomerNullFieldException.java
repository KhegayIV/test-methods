package ru.nsu.fit.endpoint.service.database.exceptions.Customer;

/**
 * Created on 09.10.2016.
 */
public class CustomerNullFieldException extends CustomerDataException {
    public CustomerNullFieldException(String fieldName) {
        super(fieldName + " should be specified");
    }
}
