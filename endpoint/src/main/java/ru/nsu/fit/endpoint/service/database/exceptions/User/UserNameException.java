package ru.nsu.fit.endpoint.service.database.exceptions.User;

/**
 * Created on 09.10.2016.
 */
public class UserNameException extends UserDataException {
    public UserNameException() {
    }

    public UserNameException(String s) {
        super(s);
    }

    public UserNameException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserNameException(Throwable cause) {
        super(cause);
    }
}
