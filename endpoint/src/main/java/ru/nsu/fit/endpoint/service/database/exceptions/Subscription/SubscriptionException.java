package ru.nsu.fit.endpoint.service.database.exceptions.Subscription;

import ru.nsu.fit.endpoint.service.database.exceptions.BaseException;

public class SubscriptionException extends BaseException
{
    protected SubscriptionException(String mess)
    {
        super(mess);
    }
}
