package ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan;

public class ServicePlanCostRangeException extends ServicePlanException
{
    public  ServicePlanCostRangeException()
    {
        super("Cost should be between 0 and 999999");
    }
}

