package ru.nsu.fit.endpoint.service.database.exceptions;

public class IllegalSeatsException extends BaseException
{
    public IllegalSeatsException()
    {
        super("Max seats should be grater or equal then min seats");
    }
}
