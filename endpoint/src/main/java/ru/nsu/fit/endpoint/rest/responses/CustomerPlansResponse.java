package ru.nsu.fit.endpoint.rest.responses;

import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.util.ArrayList;
import java.util.List;

public class CustomerPlansResponse {
    private List<Plan> plans = null;

    public CustomerPlansResponse(List<Plan> plans){
        this.plans = new ArrayList<>(plans);
    }

    @Override
    public String toString(){
        String res = "{\n" ;
        for (int i = 0; i < plans.size(); i++){
            res += "\"plan\"{\n" +
                    "\"id\":\"" + plans.get(i).getId().toString() + "\",\n" +
                    "\"name\":\"" + plans.get(i).getData().getName() + "\",\n" +
                    "\"details\":\"" + plans.get(i).getData().getDetails() + "\",\n" +
                    "\"maxSeats\":\"" + plans.get(i).getData().getMaxSeats() + "\",\n" +
                    "\"minSeats\":\"" + plans.get(i).getData().getMinSeats() + "\",\n" +
                    "\"feePerUnit\":\"" + plans.get(i).getData().getFeePerUnit() + "\"\n" +
                    "},\n";
        }
        //TODO удалить последнюю запятую
        res += "\n}";
        return res;
    }
}
