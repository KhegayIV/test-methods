package ru.nsu.fit.endpoint;

import java.util.regex.Pattern;

/**
 * Created on 09.10.2016.
 */
public class AccountUtils {

    private static final Pattern namePattern = Pattern.compile("\\p{Upper}(\\p{Lower}){1,11}");
    /* For this project use simple pattern, not that enormous one*/
    private static final Pattern emailPattern = Pattern.compile("^[\\w\\-\\.]+\\@(\\p{Alpha}+\\.)+\\p{Alpha}{2,3}$");

    private static final String[] easyPassParts = new String[]{"qwe", "qwer", "qwert", "qwerty", "1", "2", "3"};
    private static final Pattern easyPassPattern;
    static {
        easyPassPattern = Pattern.compile("^("+String.join("|", easyPassParts)+")+$");
    }

    private AccountUtils(){}

    public static boolean isPasswordEasy(final String password){
        return easyPassPattern.matcher(password).matches();
    }

    public static boolean doesPasswordContain(final String pass, final String s){
        return pass != null && pass.toLowerCase().contains(s.toLowerCase());
    }

    public static boolean isStringValidName(final String s){
        return namePattern.matcher(s).matches();
    }

    public static boolean isEmail(final String s){
        return emailPattern.matcher(s).matches();
    }
}
