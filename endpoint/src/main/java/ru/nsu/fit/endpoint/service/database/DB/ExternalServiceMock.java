package ru.nsu.fit.endpoint.service.database.DB;

import ru.nsu.fit.endpoint.service.database.data.Plan;

import javax.enterprise.inject.Alternative;
import java.util.Random;

@Alternative
public class ExternalServiceMock implements IExternalService {

    @Override
    public boolean IsExternal(String planName) {
        return planName.startsWith("EXT");
    }

    @Override
    public void BuyExternalPlan(String customerId, Plan plan) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
