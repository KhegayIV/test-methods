package ru.nsu.fit.endpoint.service.database.enums;

public enum UserRole {
    COMPANY_ADMINISTRATOR("Company administrator"),
    TECHNICAL_ADMINISTRATOR("Technical administrator"),
    BILLING_ADMINISTRATOR("Billing administrator"),
    USER("User");

    private String roleName;

    UserRole(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }
}
