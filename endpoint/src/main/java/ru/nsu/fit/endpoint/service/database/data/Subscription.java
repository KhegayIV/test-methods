package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import ru.nsu.fit.endpoint.service.database.enums.SubscriptionStatus;
import ru.nsu.fit.endpoint.service.database.exceptions.IllegalSeatsException;
import ru.nsu.fit.endpoint.service.database.exceptions.MaxSeatsRangeException;
import ru.nsu.fit.endpoint.service.database.exceptions.MinSeatsRangeException;
import ru.nsu.fit.endpoint.service.database.exceptions.Subscription.SubscriptionIllegalUsedSeatsException;
import ru.nsu.fit.endpoint.service.database.exceptions.Subscription.SubscriptionUsedSeatsRangeException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Subscription extends Entity<Subscription.SubscriptionData>  {
    private UUID id;

    public Subscription(SubscriptionData subscriptionData, UUID id)
    {
        super(subscriptionData);
        this.id = id;
    }

    public Subscription(UUID customerId, UUID servicePlanId, int maxSeats, int minSeats, int usedSeats, SubscriptionStatus status)
    {
        super(new SubscriptionData(customerId, servicePlanId, maxSeats, minSeats, usedSeats, status));
        this.id = UUID.randomUUID();
    }

    public void setStatus(SubscriptionStatus status)
    {
        data.setStatus(status);
    }

    public UUID getId(){return id;}

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class SubscriptionData {
        @JsonProperty("customerId")
        @SerializedName("customerId")
        private UUID customerId;
        @JsonProperty("servicePlanId")
        @SerializedName("servicePlanId")
        private UUID servicePlanId;
        @JsonProperty("maxSeats")
        @SerializedName("maxSeats")
        private int maxSeats = 999999;
        @JsonProperty("minSeats")
        @SerializedName("minSeats")
        private int minSeats = 1;
        @JsonProperty("usedSeats")
        @SerializedName("usedSeats")
        private int usedSeats = 0;
        @JsonProperty("status")
        @SerializedName("status")
        private SubscriptionStatus status = null;

        public UUID getCustomerId() {
            return customerId;
        }

        public UUID getServicePlanId() {
            return servicePlanId;
        }

        public int getMaxSeats() {
            return maxSeats;
        }

        public int getMinSeats() {
            return minSeats;
        }

        public int getUsedSeats() {
            return usedSeats;
        }

        public SubscriptionStatus getStatus() {
            return status;
        }

        public SubscriptionData(UUID customerId, UUID servicePlanId, int maxSeats, int minSeats, int usedSeats, SubscriptionStatus status)
        {
            this.customerId = customerId;
            this.servicePlanId = servicePlanId;
            setMaxSeats(maxSeats);
            setMinSeats(minSeats);
            setUsedSeats(usedSeats);
            setStatus(status);
        }

        @Override
        public String toString() {
            return "SubscriptionData{" +
                    "customerId='" + customerId + '\'' +
                    ", servicePlanId='" + servicePlanId + '\'' +
                    ", maxSeats=" + maxSeats +
                    ", minSeats=" + minSeats +
                    ", usedSeats=" + usedSeats +
                    "status='" + status.getStatus() + '\'' +
                    '}';
        }

        protected void setStatus(SubscriptionStatus status)
        {
            this.status = status;
        }

        private void setMaxSeats(int maxSeats) {
            if (maxSeats < 1 || maxSeats > 999999)
                throw new MaxSeatsRangeException();
            if (maxSeats < minSeats)
                throw new IllegalSeatsException();

            this.maxSeats = maxSeats;
        }

        private void setMinSeats(int minSeats) {
            if (minSeats < 1 || minSeats > 999999)
                throw new MinSeatsRangeException();
            if (maxSeats < minSeats)
                throw new IllegalSeatsException();

            this.minSeats = minSeats;
        }

        private void setUsedSeats(int usedSeats) {
            if (usedSeats < 0 || usedSeats > 999999)
                throw new SubscriptionUsedSeatsRangeException();
            if (usedSeats > maxSeats)
                throw new SubscriptionIllegalUsedSeatsException();

            this.usedSeats = usedSeats;
        }
    }
}
