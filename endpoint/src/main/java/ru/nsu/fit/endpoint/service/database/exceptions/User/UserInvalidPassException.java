package ru.nsu.fit.endpoint.service.database.exceptions.User;

/**
 * Created on 09.10.2016.
 */
public class UserInvalidPassException extends UserDataException {
    public UserInvalidPassException() {
    }

    public UserInvalidPassException(String s) {
        super(s);
    }

    public UserInvalidPassException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserInvalidPassException(Throwable cause) {
        super(cause);
    }
}
