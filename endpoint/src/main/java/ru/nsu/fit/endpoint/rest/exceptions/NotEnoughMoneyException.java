package ru.nsu.fit.endpoint.rest.exceptions;

import ru.nsu.fit.endpoint.service.database.exceptions.BaseException;

public class NotEnoughMoneyException extends BaseException
{
    public NotEnoughMoneyException()
    {
        super("Not enough money to buy");
    }
}
