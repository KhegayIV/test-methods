package ru.nsu.fit.endpoint.service.database.exceptions.Subscription;

public class SubscriptionUsedSeatsRangeException extends SubscriptionException
{
    public SubscriptionUsedSeatsRangeException()
    {
        super("Used seats should be between 1 and 999999");
    }
}
