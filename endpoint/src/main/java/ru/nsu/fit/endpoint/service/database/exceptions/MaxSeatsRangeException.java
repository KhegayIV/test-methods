package ru.nsu.fit.endpoint.service.database.exceptions;

public class MaxSeatsRangeException extends BaseException
{
    public MaxSeatsRangeException()
    {
        super("Max seats should be between 1 and 999999");
    }
}
