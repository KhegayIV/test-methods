package ru.nsu.fit.endpoint.rest.responses;

import ru.nsu.fit.endpoint.service.database.data.Customer;

import java.util.ArrayList;
import java.util.List;

public class CustomersResponse {
    private List<Customer> customers = null;

    public  CustomersResponse(List<Customer> customers){
        this.customers = new ArrayList<>(customers);
    }

    @Override
    public String toString(){
        String res = "{\n";
        for(int i = 0 ; i < customers.size() ; i++){
            res += "\"customer\":{\n" +
                    "\"id\":\"" + customers.get(i).getId().toString() + "\",\n" +
                    "\"firstname\":\"" + customers.get(i).getData().getFirstName() + "\",\n" +
                    "\"lastname\":\"" + customers.get(i).getData().getLastName() + "\",\n" +
                    "\"login\":\"" + customers.get(i).getData().getLogin() + "\",\n" +
                    "\"money\":\"" + customers.get(i).getData().getMoney() + "\",\n" +
            "},";
        }

        //TODO удалить последнюю запятую
        res += "\n}";
        return res;
    }

}
