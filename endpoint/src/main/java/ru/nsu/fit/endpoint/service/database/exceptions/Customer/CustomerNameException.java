package ru.nsu.fit.endpoint.service.database.exceptions.Customer;

/**
 * Created on 09.10.2016.
 */
public class CustomerNameException extends CustomerDataException {
    public CustomerNameException() {
    }

    public CustomerNameException(String message) {
        super(message);
    }

    public CustomerNameException(String message, Throwable cause) {
        super(message, cause);
    }

    public CustomerNameException(Throwable cause) {
        super(cause);
    }

}
