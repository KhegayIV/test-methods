package ru.nsu.fit.endpoint.service.database.DB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.service.database.enums.SubscriptionStatus;
import ru.nsu.fit.endpoint.service.database.enums.UserRole;

import javax.ejb.Asynchronous;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Named
@RequestScoped
public class DBService{
    // Constants
    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, money) values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String INSERT_CUSTOMER_PLAN = "INSERT INTO CUSTOMER_PLAN(customer_id, plan_id) VALUES ('%s', '%s')";
    private static final String INSERT_USER = "INSERT INTO USER(id, customer_id, first_name, last_name, login, pass, user_role) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
    private static final String INSERT_USER_SUBSCRIPTION = "INSERT INTO USER_ASSIGNMENT(user_id, subscription_id) VALUES ('%s', '%s')";
    private static final String INSERT_PLAN = "INSERT INTO PLAN(id, name, details, min_seats, max_seats, fee_per_seat, cost) VALUES ('%s','%s','%s','%s','%s','%s','%s')";
    private static final String INSERT_SUBSCRIPTION = "INSERT INTO SUBSCRIPTION(id, plan_id, customer_id, min_seats, max_seats, used_seats, status) VALUES ('%s','%s','%s', %s, %s, %s, '%s')";


    private static final String UPDATE_CUSTOMER_MONEY = "UPDATE CUSTOMER SET money=%s WHERE id='%s'";
    private static final String UPDATE_USER_ROLE = "UPDATE USER SET user_role='%s' WHERE id='%s'";
    private static final String UPDATE_SUBSCRIPTION_USED_SEATS = "UPDATE SUBSCRIPTION SET used_seats=%s WHERE id='%s'";
    private static final String UPDATE_SUBSCRIPTION_STATUS = "UPDATE SUBSCRIPTION SET status='%s' WHERE id='%s'";

    private static final String DELETE_USER_SUBSCRIPTION = "DELETE FROM USER_ASSIGNMENT WHERE user_id='%s' AND subscription_id='%s'";
    private static final String DELETE_USER = "DELETE FROM USER WHERE id='%s'";
    private static final String DELETE_CUSTOMER = "DELETE FROM CUSTOMER WHERE id='%s'";
    private static final String DELETE_PLAN = "DELETE FROM PLAN WHERE id='%s'";

    private static final String SELECT_CUSTOMER_ID = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_CUSTOMER_DATA_BY_ID = "SELECT first_name, last_name, login, pass, money FROM CUSTOMER WHERE id='%s'";
    private static final String SELECT_USERS_BY_CUSTOMER_ID = "SELECT * FROM USER WHERE customer_id='%s'";
    private static final String SELECT_PLANS_BY_CUSTOMER_ID = "SELECT * FROM CUSTOMER_PLAN WHERE customer_id='%s'";
    private static final String SELECT_PLAN_BY_ID = "SELECT * FROM PLAN WHERE id='%s'";
    private static final String SELECT_SUBSCRIPTIONS_BY_CUSTOMER_ID = "SELECT * FROM SUBSCRIPTION WHERE customer_id='%s'";
    private static final String SELECT_CUSTOMER_MONEY = "SELECT money FROM CUSTOMER WHERE id='%s'";
    private static final String SELECT_SUBSCRIPTION_BY_ID = "SELECT * FROM SUBSCRIPTION WHERE id='%s'";
    private static final String SELECT_USER_BY_ID = "SELECT * FROM USER WHERE id='%s'";
    private static final String SELECT_USER_ID = "SELECT id FROM USER WHERE login='%s'";
    private static final String SELECT_USER_ROLE = "SELECT user_role FROM USER WHERE login='%s'";
    private static final String SELECT_USER_SUBSCRIPTION = "SELECT * FROM USER_ASSIGNMENT WHERE user_id='%s' AND subscription_id='%s'";
    private static final String SELECT_USER_SUBSCRIPTIONS = "SELECT * FROM SUBSCRIPTION INNER JOIN (SELECT subscription_id FROM USER_ASSIGNMENT WHERE user_id='%s') as s_id ON SUBSCRIPTION.id = s_id.subscription_id";
    private static final String SELECT_CUSTOMER_PASS = "SELECT pass FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_USER_PASS = "SELECT pass FROM USER WHERE login='%s'";
    private static final String SELECT_ALL_CUSTOMERS = "SELECT * FROM CUSTOMER";

    private static final Logger logger = LoggerFactory.getLogger("DB_LOG");
    private static final Object generalMutex = new Object();
    private static Connection connection;

    static {
        init();
    }

    public static Customer createCustomer(Customer.CustomerData customerData) {
        synchronized (generalMutex) {
            logger.info("Try to create customer");

            Customer customer = new Customer(customerData, UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER,
                                customer.getId(),
                                customer.getData().getFirstName(),
                                customer.getData().getLastName(),
                                customer.getData().getLogin(),
                                customer.getData().getPass(),
                                customer.getData().getMoney()));
                return customer;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static void fillAccount(String customerId, int money) {
        int customerMoney = getCustomerMoney(customerId);
        setCustomerMoney(customerId, customerMoney + money);
    }

    public static int getCustomerMoney(String customerId) {
        synchronized (generalMutex) {
            logger.info("Try to get customer's money");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_MONEY,
                                customerId));
                if (rs.next()) {
                    return Integer.parseInt(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerId + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static Plan getPlan(String planId){
        synchronized (generalMutex) {
            logger.info("Try to get Plan");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_PLAN_BY_ID,
                                planId));
                if (rs.next()) {
                    return new Plan(new Plan.PlanData(rs.getString(2), rs.getString(3), Integer.parseInt(rs.getString(5)), Integer.parseInt(rs.getString(4)), Integer.parseInt(rs.getString(6)), Integer.parseInt(rs.getString(7))), UUID.fromString(rs.getString(1)));
                } else {
                    throw new IllegalArgumentException("Plan with id '" + planId + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    
    public static void buyPlan(String customerId, String planId) {
        synchronized (generalMutex) {
            logger.info("Try to buy plan");

            Plan plan = getPlan(planId);

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER_PLAN,
                                customerId,
                                planId));
                if(ExternalServiceProvider.getService().IsExternal(plan.getData().getName()))
                    buyPlanAsync(customerId, plan);
                else
                    buyPlanSync(customerId, plan, statement);
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private static void buyPlanSync(String customerId, Plan plan, Statement statement) throws SQLException {
        logger.info("Try to buy internal plan");
        int customerMoney = getCustomerMoney(customerId);
        statement.executeUpdate(
                String.format(
                        INSERT_SUBSCRIPTION,
                        UUID.randomUUID().toString(),
                        plan.getId().toString(),
                        customerId,
                        plan.getData().getMinSeats(),
                        plan.getData().getMaxSeats(),
                        0,
                        SubscriptionStatus.DONE_STATUS
                )
        );
        setCustomerMoney(customerId, customerMoney - plan.getData().getCost());
    }

    @Asynchronous
    private static void buyPlanAsync(String customerId, Plan plan){
        new Thread( () -> {
            logger.info("Try to buy external plan");
            int customerMoney = getCustomerMoney(customerId);

            try {
                Statement statement = connection.createStatement();
                String newGuid = UUID.randomUUID().toString();
                synchronized (generalMutex) {
                    statement.executeUpdate(
                            String.format(
                                    INSERT_SUBSCRIPTION,
                                    newGuid,
                                    plan.getId().toString(),
                                    customerId,
                                    plan.getData().getMinSeats(),
                                    plan.getData().getMaxSeats(),
                                    0,
                                    SubscriptionStatus.PROVISIONING_STATUS
                            )
                    );
                }
                setCustomerMoney(customerId, customerMoney - plan.getData().getCost());
                ExternalServiceProvider.getService().BuyExternalPlan(customerId, plan);
                synchronized (generalMutex) {
                    statement.executeUpdate(
                            String.format(
                                    UPDATE_SUBSCRIPTION_STATUS,
                                    SubscriptionStatus.DONE_STATUS,
                                    newGuid
                            )
                    );
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }).run();
    }


    public static User createUser(User.UserData userData) {
        synchronized (generalMutex) {
            logger.info("Try to create user");

            User user = new User(userData, UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_USER,
                                user.getId(),
                                user.getData().getCustomerId(),
                                user.getData().getFirstName(),
                                user.getData().getLastName(),
                                user.getData().getLogin(),
                                user.getData().getPass(),
                                user.getData().getUserRole()));
                return user;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    
    public static void deleteUser(String userId) {
        synchronized (generalMutex) {
            logger.info("Try to delete user");

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                DELETE_USER,
                                userId
            ));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static Subscription getSubscription(String subscriptionId){
        synchronized (generalMutex) {
            logger.info("Try to get subscription");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_SUBSCRIPTION_BY_ID,
                                subscriptionId));
                if (rs.next()) {
                    return new Subscription(new Subscription.SubscriptionData(UUID.fromString(rs.getString(3)), UUID.fromString(rs.getString(2)), Integer.parseInt(rs.getString(5)), Integer.parseInt(rs.getString(4)), Integer.parseInt(rs.getString(6)), SubscriptionStatus.valueOf(rs.getString(7))), UUID.fromString(rs.getString(1)));
                } else {
                    throw new IllegalArgumentException("Subscription with id '" + subscriptionId + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private static void setCustomerMoney(String customerId, int money)
    {
        synchronized (generalMutex) {
            logger.info("Try to set customer's account");

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                UPDATE_CUSTOMER_MONEY,
                                money,
                                customerId));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    
    public static User getUser(String userId) {
        synchronized (generalMutex) {
            logger.info("Try to get user");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_BY_ID,
                                userId));
                if(rs.next()){
                return new User(new User.UserData(rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), UserRole.valueOf(rs.getString(7)), UUID.fromString(rs.getString(2))), UUID.fromString(rs.getString(1)));
                } else {
                    throw new IllegalArgumentException("User with id '" + userId + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    
    public static UUID getUserIdByLogin(String userLogin){
        synchronized (generalMutex) {
            logger.info("Try to get user login");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_ID,
                                userLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("User with login '" + userLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    
    public static UserRole getUserRoleByLogin(String userLogin){
        synchronized (generalMutex) {
            logger.info("Try to get user role");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_ROLE,
                                userLogin));
                if (rs.next()) {
                    return UserRole.valueOf(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("Role of user with login '" + userLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }


    
    public static List<Subscription> getUserSubscriptionsByLogin(String userLogin){
        synchronized (generalMutex) {
            logger.info("Try to get user subscriptions");

            try {
                Statement statement = connection.createStatement();
                String userId = getUserIdByLogin(userLogin).toString();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_SUBSCRIPTIONS,
                                userId));
                List<Subscription> subscriptions = new ArrayList<Subscription>();
                while (rs.next()) {
                    subscriptions.add(new Subscription(new Subscription.SubscriptionData(UUID.fromString(rs.getString(3)), UUID.fromString(rs.getString(2)), Integer.parseInt(rs.getString(5)), Integer.parseInt(rs.getString(4)), Integer.parseInt(rs.getString(6)), SubscriptionStatus.valueOf(rs.getString(7))), UUID.fromString(rs.getString(1))));
                }
                return subscriptions;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static List<Customer> getAllCustomers(){
        synchronized (generalMutex) {
            logger.info("Try to get customers");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        SELECT_ALL_CUSTOMERS);
                List<Customer> customers = new ArrayList<Customer>();
                while (rs.next()) {
                    customers.add(new Customer(new Customer.CustomerData(rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), Integer.parseInt(rs.getString(6))), UUID.fromString(rs.getString(1))));
                }
                return customers;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static boolean isSubscriptionAssigned(String userId, String subscriptionId){
        synchronized (generalMutex) {
            logger.info("Try to find user subscription");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_SUBSCRIPTION,
                                userId,
                                subscriptionId));
                if(rs.next()){
                    return true;
                } else {
                    return false;
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    
    public static void assignSubscription(String userId, String subscriptionId) {
        synchronized (generalMutex) {
            logger.info("Try to assign subscription");
            Subscription subscription = getSubscription(subscriptionId);
            int customerMoney = getCustomerMoney(subscription.getData().getCustomerId().toString());
            Plan plan = getPlan(subscription.getData().getServicePlanId().toString());

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_USER_SUBSCRIPTION,
                                userId.toString(),
                                subscriptionId.toString()
                                ));
                statement.executeUpdate(
                        String.format(
                                UPDATE_SUBSCRIPTION_USED_SEATS,
                                subscription.getData().getUsedSeats()+1,
                                subscriptionId.toString()
                        ));
                setCustomerMoney(subscription.getData().getCustomerId().toString(), customerMoney-plan.getData().getFeePerUnit());
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    
    public static void reassignSubscription(String userId, String subscriptionId) {
        synchronized (generalMutex) {
            logger.info("Try to reassign subscription");

            try {
                Statement statement = connection.createStatement();

                statement.executeUpdate(
                        String.format(
                                DELETE_USER_SUBSCRIPTION,
                                userId.toString(),
                                subscriptionId.toString()
                        ));
                Subscription subscription = getSubscription(subscriptionId);

                statement.executeUpdate(
                        String.format(
                                UPDATE_SUBSCRIPTION_USED_SEATS,
                                subscription.getData().getUsedSeats()-1,
                                subscriptionId.toString()
                        ));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    
    public static void assignUserRole(String userId, UserRole userRole) {
        synchronized (generalMutex) {
            logger.info("Try to assign user role");

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                UPDATE_USER_ROLE,
                                userRole,
                                userId.toString()));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }


    
    public static UUID getCustomerIdByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info("Try to find customer's id");
            return findCustomerIdByLogin(customerLogin);
        }
    }

    private static UUID findCustomerIdByLogin(String customerLogin)
    {
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(
                    String.format(
                            SELECT_CUSTOMER_ID,
                            customerLogin));
            if (rs.next()) {
                return UUID.fromString(rs.getString(1));
            } else {
                throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
            }
        } catch (SQLException ex) {
            logger.debug(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    public static String getCustomerLoginByUserId(String userId)
    {
        User.UserData userData = getUser(userId).getData();
        String customerId = userData.getCustomerId().toString();
        Customer.CustomerData customerData = getCustomerDataById(customerId);
        return customerData.getLogin();
    }

    
    public static Customer.CustomerData getCustomerDataById(String customerId) {
        synchronized (generalMutex) {
            logger.info("Try to find customer");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_DATA_BY_ID,
                                customerId));
                if (rs.next()) {
                    return new Customer.CustomerData(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), Integer.parseInt(rs.getString(5)));
                } else {
                    throw new IllegalArgumentException("Customer with id '" + customerId + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    
    public static List<User> getUsersByCustomerId(String customerId) {
        synchronized (generalMutex) {
            logger.info("Try to find customer's users");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USERS_BY_CUSTOMER_ID,
                                customerId));
                List<User> users = new ArrayList<>();
                while(rs.next())
                {
                    users.add(new User(new User.UserData(rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), UserRole.valueOf(rs.getString(7)), UUID.fromString(rs.getString(2))), UUID.fromString(rs.getString(1))));
                }
                return  users;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }

    }

    
    public static List<Plan> getPlansByCustomerId(String customerId) {
        synchronized (generalMutex) {
            logger.info("Try to find customer's plans");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_PLANS_BY_CUSTOMER_ID,
                                customerId));
                List<Plan> plans = new ArrayList<Plan>();
                while (rs.next()) {
                    plans.add(new Plan(new Plan.PlanData(rs.getString(2), rs.getString(3), Integer.parseInt(rs.getString(5)), Integer.parseInt(rs.getString(4)), Integer.parseInt(rs.getString(6)), Integer.parseInt(rs.getString(7))), UUID.fromString(rs.getString(1))));
                }
                return plans;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    
    public static List<Subscription> getSubscriptionsByCustomerId(String customerId) {
        synchronized (generalMutex) {
            logger.info("Try to find customer's subscriptions");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_SUBSCRIPTIONS_BY_CUSTOMER_ID,
                                customerId));
                List<Subscription> subscriptions = new ArrayList<Subscription>();
                while (rs.next()) {
                    subscriptions.add(new Subscription(new Subscription.SubscriptionData(UUID.fromString(rs.getString(3)), UUID.fromString(rs.getString(2)), Integer.parseInt(rs.getString(5)), Integer.parseInt(rs.getString(4)), Integer.parseInt(rs.getString(6)), SubscriptionStatus.valueOf(rs.getString(7))), UUID.fromString(rs.getString(1))));
                }
                return subscriptions;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }

    }

    public static String getCustomerPassword(String customerLogin) {
        synchronized (generalMutex) {
            logger.info("Try to get customer's password");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_PASS,
                                customerLogin));
                if (rs.next()) {
                    return rs.getString(1);
                } else {
                    return null;
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static String getUserPassword(String userLogin) {
        synchronized (generalMutex) {
            logger.info("Try to get user's password");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_USER_PASS,
                                userLogin));
                if (rs.next()) {
                    return rs.getString(1);
                } else {
                        return null;
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private static void init() {
        logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://localhost:3306/testmethods?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false",
                            "user",
                            "user");
        } catch (SQLException ex) {
            logger.debug("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.debug("Failed to make connection!");
        }
    }

    public static void deleteCustomer(String id) {
        synchronized (generalMutex) {
            logger.info("Try to delete customer");

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                DELETE_CUSTOMER,
                                id
                        ));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public static Plan createPlan(Plan.PlanData planData){
        Plan plan = new Plan(planData, UUID.randomUUID());
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(
                    String.format(
                            INSERT_PLAN,
                            plan.getId(),
                            plan.getData().getName(),
                            plan.getData().getDetails(),
                            plan.getData().getMaxSeats(),
                            plan.getData().getMaxSeats(),
                            plan.getData().getFeePerUnit(),
                            plan.getData().getCost()));
            return plan;
        } catch (SQLException ex) {
            logger.debug(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    public static void deletePlan(String  planId){
        logger.info("Try to delete customer");

        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(
                    String.format(
                            DELETE_PLAN,
                            planId
                    ));
        } catch (SQLException ex) {
            logger.debug(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }
}
