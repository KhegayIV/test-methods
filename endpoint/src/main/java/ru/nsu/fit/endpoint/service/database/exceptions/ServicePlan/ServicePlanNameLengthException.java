package ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan;

public class ServicePlanNameLengthException extends ServicePlanException
{
    public ServicePlanNameLengthException()
    {
        super("Name's length should be more or equal 2 symbols and less or equal 128 symbols");
    }
}
