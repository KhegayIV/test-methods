package ru.nsu.fit.endpoint.rest.responses;

import ru.nsu.fit.endpoint.service.database.data.User;

import java.util.ArrayList;
import java.util.List;

public class UsersResponse {
    private List<User> users = null;

    public UsersResponse(List<User> users){
        this.users = new ArrayList<>(users);
    }

    @Override
    public String toString(){
        String res = "{\n";
        for(int i = 0 ; i < users.size() ; i++){
            res += "\"user\":{\n" +
                    "\"id\":\"" + users.get(i).getId().toString() + "\",\n" +
                    "\"firstName\":\"" + users.get(i).getData().getFirstName() + "\",\n" +
                    "\"lastName\":\"" + users.get(i).getData().getLastName() + "\",\n" +
                    "\"login\":\"" + users.get(i).getData().getLogin() + "\",\n" +
                    "\"password\":\"" + users.get(i).getData().getPass() + "\",\n" +
                    "\"role\":\"" + users.get(i).getData().getUserRole() + "\"\n" +
                    "},";
        }

        //TODO удалить последнюю запятую
        res += "\n}";
        return res;
    }
}
