package ru.nsu.fit.endpoint.service.database.exceptions.User;

/**
 * Created on 09.10.2016.
 */
public class UserDataException extends IllegalArgumentException {
    public UserDataException() {
    }

    public UserDataException(String s) {
        super(s);
    }

    public UserDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserDataException(Throwable cause) {
        super(cause);
    }
}
