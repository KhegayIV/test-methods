package ru.nsu.fit.endpoint.service.database.exceptions;

public class BaseException extends RuntimeException
{
    public BaseException(String mess)
    {
        super(mess);
    }
}
