package ru.nsu.fit.endpoint.rest.exceptions;

import ru.nsu.fit.endpoint.service.database.exceptions.BaseException;

public class NotConfirmedSubscriptionException extends BaseException {
    public NotConfirmedSubscriptionException() {
        super("Operation aborted. You try to use not confirmed subscription");
    }
}
