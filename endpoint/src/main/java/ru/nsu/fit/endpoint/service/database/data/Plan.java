package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import ru.nsu.fit.endpoint.service.database.exceptions.IllegalSeatsException;
import ru.nsu.fit.endpoint.service.database.exceptions.MaxSeatsRangeException;
import ru.nsu.fit.endpoint.service.database.exceptions.MinSeatsRangeException;
import ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan.*;

import java.util.UUID;
import java.util.regex.Pattern;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Plan extends Entity<Plan.PlanData> {
    private UUID id;

    public Plan(PlanData planData, UUID id)
    {
        super(planData);
        this.id = id;
    }

    public Plan(String name, String details, int maxSeats, int minSeats, int feePerUnit, int cost)
    {
        super(new PlanData(name, details, maxSeats, minSeats, feePerUnit, cost));
        id = UUID.randomUUID();
    }

    public UUID getId(){return id;}

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PlanData {

        /* Длина не больше 128 символов и не меньше 2 включительно не содержит спец символов */
        @JsonProperty("name")
        @SerializedName("name")
        private String name;
        /* Длина не больше 1024 символов и не меньше 1 включительно */
        @JsonProperty("details")
        @SerializedName("details")
        private String details;
        /* Не больше 999999 и не меньше 1 включительно */
        @JsonProperty("maxSeats")
        @SerializedName("maxSeats")
        private int maxSeats = 999999;
        /* Не больше 999999 и не меньше 1 включительно, minSeats >= maxSeats */
        @JsonProperty("minSeats")
        @SerializedName("minSeats")
        private int minSeats = 1;
        /* Больше ли равно 0 но меньше либо равно 999999 */
        @JsonProperty("feePerUnit")
        @SerializedName("feePerUnit")
        private int feePerUnit;
        @JsonProperty("cost")
        @SerializedName("cost")
        private int cost;

        public PlanData(String name, String details, int maxSeats, int minSeats, int feePerUnit, int cost)
        {
            setName(name);
            setDetails(details);
            setMinSeats(minSeats);
            setMaxSeats(maxSeats);
            setFeePerUnit(feePerUnit);
            setCost(cost);
        }

        public int getFeePerUnit() {
            return feePerUnit;
        }

        public int getMinSeats() {
            return minSeats;
        }

        public int getMaxSeats() {
            return maxSeats;
        }

        public String getDetails() {
            return details;
        }

        public String getName() {
            return name;
        }

        public int getCost() { return cost; }

        @Override
        public String toString() {
            return "PlanData{" +
                    "name='" + name + '\'' +
                    ", details='" + details + '\'' +
                    ", maxSeats=" + maxSeats +
                    ", minSeats=" + minSeats +
                    ", feePerUnit=" + feePerUnit +
                    ", cost=" + cost +
                    '}';
        }

        private void setName(String name) {
            if (name.length() < 2 || name.length() > 128)
                throw new ServicePlanNameLengthException();
            Pattern p = Pattern.compile("[^a-zа-я0-9 ]", Pattern.CASE_INSENSITIVE);
            if (p.matcher(name).find())
                throw new ServicePlanNameWrongContentException();

            this.name = name;
        }

        private void setDetails(String details) {
            if (details.length() < 1 || details.length() > 1024)
                throw new ServicePlanDetailsLengthException();

            this.details = details;
        }

        private void setMaxSeats(int maxSeats) {
            if (maxSeats < 1 || maxSeats > 999999)
                throw new MaxSeatsRangeException();
            if (maxSeats < minSeats)
                throw new IllegalSeatsException();

            this.maxSeats = maxSeats;
        }

        private void setMinSeats(int minSeats) {
            if (minSeats < 1 || minSeats > 999999)
                throw new MinSeatsRangeException();
            if (maxSeats < minSeats)
                throw new IllegalSeatsException();

            this.minSeats = minSeats;
        }

        private void setFeePerUnit(int feePerUnit) {
            if (feePerUnit < 0 || feePerUnit > 999999)
                throw new ServicePlanFeePerUnitRangeException();

            this.feePerUnit = feePerUnit;
        }

        private void setCost(int cost){
            if (cost < 0 || cost > 999999)
                throw  new ServicePlanCostRangeException();

            this.cost = cost;
        }
    }
}
