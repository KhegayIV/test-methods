package ru.nsu.fit.endpoint.service.database.exceptions;

public class MinSeatsRangeException extends BaseException
{
    public MinSeatsRangeException()
    {
        super("Min seats should be between 1 and 999999");
    }
}
