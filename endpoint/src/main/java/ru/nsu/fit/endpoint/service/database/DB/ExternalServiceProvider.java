package ru.nsu.fit.endpoint.service.database.DB;

/**
 * Created on 27.11.2016.
 */
public class ExternalServiceProvider {
    private static IExternalService service;


    public static IExternalService getService() {
        return service;
    }

    public static void setService(IExternalService service) {
        ExternalServiceProvider.service = service;
    }
}
