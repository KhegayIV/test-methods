package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.nsu.fit.endpoint.AccountUtils;
import ru.nsu.fit.endpoint.service.database.exceptions.Customer.CustomerInvalidPassException;
import ru.nsu.fit.endpoint.service.database.exceptions.Customer.CustomerNameException;
import ru.nsu.fit.endpoint.service.database.exceptions.Customer.CustomerNegativeMoneyException;
import ru.nsu.fit.endpoint.service.database.exceptions.Customer.CustomerNullFieldException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */

public class Customer extends Entity<Customer.CustomerData> {
    private UUID id;

    public Customer(CustomerData data, UUID id) {
            super(data);
            this.id = id;
    }
    public Customer(String firstName, String lastName, String login, String pass, int money) {
        super(new CustomerData(firstName, lastName, login, pass, money));
        this.id = UUID.randomUUID();
    }
    public UUID getId() {
            return id;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CustomerData {
        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        @JsonProperty("firstName")
        private String firstName;

        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        @JsonProperty("lastName")
        private String lastName;

        /* указывается в виде email, проверить email на корректность */
        @JsonProperty("login")
        private String login;

        /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
        @JsonProperty("pass")
        private String pass;

        /* счет не может быть отрицательным */
        @JsonProperty("money")
        private int money;

        private CustomerData() {}

        public CustomerData(String firstName, String lastName, String login, String pass, int money) {
            setFirstName(firstName);
            setLastName(lastName);
            setLogin(login);
            setPass(pass);
            setMoney(money);
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getLogin() {
            return login;
        }

        public String getPass() {
            return pass;
        }

        public int getMoney() {
            return money;
        }

        @Override
        public String toString() {
            return "CustomerData{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", login='" + login + '\'' +
                    ", pass='" + pass + '\'' +
                    ", money=" + money +
                    '}';
        }

        private void setFirstName(final String firstName){
            if (firstName == null){
                throw new CustomerNullFieldException("First name");
            }
            if (firstName.length() < 2){
                throw new CustomerNameException("First name's length should be 2 symbols or more");
            }
            if (firstName.length() > 12){
                throw new CustomerNameException("First name's length should be 12 symbols or less");
            }
            if (AccountUtils.isStringValidName(firstName)){
                if (!AccountUtils.doesPasswordContain(pass, firstName)) {
                    this.firstName = firstName;
                } else {
                    throw new CustomerInvalidPassException("First name can't be part of a password");
                }
            } else {
                throw new CustomerNameException("First name has invalid format");
            }
        }

        private void setLastName(final String lastName){
            if (lastName == null){
                throw new CustomerNullFieldException("Last name");
            }
            if (lastName.length() < 2){
                throw new CustomerNameException("Last name's length should be 2 symbols or more");
            }
            if (lastName.length() > 12){
                throw new CustomerNameException("Last name's length should be 12 symbols or less");
            }
            if (AccountUtils.isStringValidName(lastName)){
                if (!AccountUtils.doesPasswordContain(pass, lastName)) {
                    this.lastName = lastName;
                } else {
                    throw new CustomerInvalidPassException("Last name can't be part of a password");
                }
            } else {
                throw new CustomerNameException("Last name has invalid format");
            }
        }

        private void setLogin(final String login){
            if (login == null){
                throw new CustomerNullFieldException("Login");
            }
            if (AccountUtils.isEmail(login)){
                if (!AccountUtils.doesPasswordContain(pass, login)) {
                    this.login = login;
                } else {
                    throw new CustomerInvalidPassException("Login can't be part of a password");
                }
            } else {
                throw new CustomerNameException("Login should be an email");
            }
        }

        private void setPass(final String password){
            if (password == null){
                throw new CustomerNullFieldException("Password");
            }
            if (password.length() < 6) {
                throw new CustomerInvalidPassException("Password's length should be 6 symbols or more");
            }
            if (password.length() > 12) {
                throw new CustomerInvalidPassException("Password's length should be 12 symbols or less");
            }
            if (AccountUtils.isPasswordEasy(password)){
                throw new CustomerInvalidPassException("Password is too easy");
            }
            if (AccountUtils.doesPasswordContain(password, firstName)){
                throw new CustomerInvalidPassException("First name can't be part of a password");
            }
            if (AccountUtils.doesPasswordContain(password, lastName)){
                throw new CustomerInvalidPassException("Last name can't be part of a password");
            }
            if (AccountUtils.doesPasswordContain(password, login)){
                throw new CustomerInvalidPassException("Login can't be part of a password");
            }
            this.pass = password;
        }

        public void setMoney(int money) {
            if (money < 0){
                throw new CustomerNegativeMoneyException();
            }
            this.money = money;
        }
    }
}
