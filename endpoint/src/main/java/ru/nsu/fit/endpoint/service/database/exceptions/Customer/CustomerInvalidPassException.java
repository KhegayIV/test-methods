package ru.nsu.fit.endpoint.service.database.exceptions.Customer;

/**
 * Created on 09.10.2016.
 */
public class CustomerInvalidPassException extends CustomerDataException {

    public CustomerInvalidPassException() {
    }

    public CustomerInvalidPassException(String message) {
        super(message);
    }

    public CustomerInvalidPassException(String message, Throwable cause) {
        super(message, cause);
    }

    public CustomerInvalidPassException(Throwable cause) {
        super(cause);
    }

}
