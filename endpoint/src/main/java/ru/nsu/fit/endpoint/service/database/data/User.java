package ru.nsu.fit.endpoint.service.database.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import ru.nsu.fit.endpoint.AccountUtils;
import ru.nsu.fit.endpoint.service.database.enums.UserRole;
import ru.nsu.fit.endpoint.service.database.exceptions.User.UserInvalidPassException;
import ru.nsu.fit.endpoint.service.database.exceptions.User.UserNameException;
import ru.nsu.fit.endpoint.service.database.exceptions.User.UserNullFieldException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class User extends Entity<User.UserData> {
    private UUID id;
    private UUID[] subscriptionIds;

    public User(UserData userData, UUID id)
    {
        super(userData);
        this.id = id;
    }

    public User(String firstName, String lastName, String login, String pass, UserRole userRole, UUID customerId, UUID[] subscriptionIds) {
        super(new UserData(firstName, lastName, login, pass, userRole, customerId));
        id = UUID.randomUUID();
        this.subscriptionIds = subscriptionIds;
    }

    public UUID getId() { return id; }

    public UUID[] getSubscriptionIds(){return subscriptionIds;}

    public void setUserRole(UserRole userRole)
    {
        data.setUserRole(userRole);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class UserData
    {
        @JsonProperty("customerId")
        @SerializedName("customerId")
        private UUID customerId;

        public UUID getCustomerId() {
            return customerId;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getLogin() {
            return login;
        }

        public String getPass() {
            return pass;
        }

        public UserRole getUserRole() {
            return userRole;
        }

        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        @JsonProperty("firstName")
        @SerializedName("firstName")
        private String firstName;
        /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
        @JsonProperty("lastName")
        @SerializedName("lastName")
        private String lastName;
        /* указывается в виде email, проверить email на корректность */
        @JsonProperty("login")
        @SerializedName("login")
        private String login;
        /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
        @JsonProperty("pass")
        @SerializedName("pass")
        private String pass;
        @JsonProperty("userRole")
        @SerializedName("userRole")
        private UserRole userRole;

        @Override
        public String toString() {
            //TODO записать subscriptionIds?
            return "UserData{" +
                    "customerId='" + customerId + '\'' +
                    ", firstName='" + firstName +'\'' +
                    ", lastName='" + lastName + '\'' +
                    ", login='" + login + '\'' +
                    ", pass='" + pass + '\'' +
                    ", userRole='" + userRole + '\'' +
                    '}';
        }

        private UserData(){}

        public UserData(String firstName, String lastName, String login, String pass, UserRole userRole, UUID customerId) {
            setFirstName(firstName);
            setLastName(lastName);
            setLogin(login);
            setPass(pass);
            this.customerId = customerId;
            this.pass = pass;
            this.userRole = userRole;
        }

        protected void setUserRole(UserRole userRole)
        {
            this.userRole = userRole;
        }

        private void setFirstName(final String firstName){
            if (firstName == null){
                throw new UserNullFieldException("First name");
            }
            if (firstName.length() < 2){
                throw new UserNameException("First name's length should be 2 symbols or more");
            }
            if (firstName.length() > 12){
                throw new UserNameException("First name's length should be 12 symbols or less");
            }
            if (AccountUtils.isStringValidName(firstName)){
                if (!AccountUtils.doesPasswordContain(pass, firstName)) {
                    this.firstName = firstName;
                } else {
                    throw new UserInvalidPassException("First name can't be part of a password");
                }
            } else {
                throw new UserNameException("First name has invalid format");
            }
        }

        private void setLastName(final String lastName){
            if (lastName == null){
                throw new UserNullFieldException("Last name");
            }
            if (lastName.length() < 2){
                throw new UserNameException("Last name's length should be 2 symbols or more");
            }
            if (lastName.length() > 12){
                throw new UserNameException("Last name's length should be 12 symbols or less");
            }
            if (AccountUtils.isStringValidName(lastName)){
                if (!AccountUtils.doesPasswordContain(pass, lastName)) {
                    this.lastName = lastName;
                } else {
                    throw new UserInvalidPassException("Last name can't be part of a password");
                }
            } else {
                throw new UserNameException("Last name has invalid format");
            }
        }

        private void setLogin(final String login){
            if (login == null){
                throw new UserNullFieldException("Login");
            }
            if (AccountUtils.isEmail(login)){
                if (!AccountUtils.doesPasswordContain(pass, login)) {
                    this.login = login;
                } else {
                    throw new UserInvalidPassException("Login can't be part of a password");
                }
            } else {
                throw new UserNameException("Login should be an email");
            }
        }

        private void setPass(final String password){
            if (password == null){
                throw new UserNullFieldException("Password");
            }
            if (password.length() < 6) {
                throw new UserInvalidPassException("Password's length should be 6 symbols or more");
            }
            if (password.length() > 12) {
                throw new UserInvalidPassException("Password's length should be 12 symbols or less");
            }
            if (AccountUtils.isPasswordEasy(password)){
                throw new UserInvalidPassException("Password is too easy");
            }
            if (AccountUtils.doesPasswordContain(password, firstName)){
                throw new UserInvalidPassException("First name can't be part of a password");
            }
            if (AccountUtils.doesPasswordContain(password, lastName)){
                throw new UserInvalidPassException("Last name can't be part of a password");
            }
            if (AccountUtils.doesPasswordContain(password, login)){
                throw new UserInvalidPassException("Login can't be part of a password");
            }
            this.pass = password;
        }

    }
}
