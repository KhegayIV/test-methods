package ru.nsu.fit.endpoint.rest;

import org.apache.commons.lang.exception.ExceptionUtils;
import ru.nsu.fit.endpoint.rest.responses.CustomerPlansResponse;
import ru.nsu.fit.endpoint.rest.responses.CustomersResponse;
import ru.nsu.fit.endpoint.rest.responses.UsersResponse;
import ru.nsu.fit.endpoint.service.database.DB.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.database.data.User;
import ru.nsu.fit.endpoint.service.database.enums.SubscriptionStatus;
import ru.nsu.fit.endpoint.service.database.enums.UserRole;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
@Path("/rest/customer")
@Singleton
public class CustomerRestService {
    @Context SecurityContext sc;

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/user")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Response createUser(String userDataJson) {
        try {
            User.UserData userData = JsonMapper.fromJson(userDataJson, User.UserData.class);
            DBService.createUser(userData);
            return Response.status(200).entity(userData.toString()).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @DELETE
    @Path("/user/{user_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response deleteUser(@PathParam("user_id") String userId) {
        try {
            /*
            String userCustomerLogin = DBService.getCustomerLoginByUserId(userId);
            if(!Objects.equals( sc.getUserPrincipal().getName(), userCustomerLogin))
                return Response.status(400).entity("Operation aborted. User are not belong to this customer").build();*/

            DBService.deleteUser(userId);
            return Response.status(200).entity("Success").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/{customer_login}/id")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response getCustomerId(@PathParam("customer_login") String customerLogin) {
        try {
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, customerLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();
            UUID id = DBService.getCustomerIdByLogin(customerLogin);
            return Response.status(200).entity("{\"id\":\"" + id.toString() + "\"}").build();
        } catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/{customer_id}/data")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response getCustomerData(@PathParam("customer_id") String customerId) {
        try{
            String customerLogin = DBService.getCustomerDataById(customerId).getLogin();
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, customerLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();
            Customer.CustomerData customerData = DBService.getCustomerDataById(customerId);
            //CustomerDataResponse response = new CustomerDataResponse(customerData);
            return Response.status(200).entity(JsonMapper.toJson(customerData, true)).build();
        }
        catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/{customer_id}/users")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response getCustomerUsers(@PathParam("customer_id") String customerId) {
        try{
            String customerLogin = DBService.getCustomerDataById(customerId).getLogin();
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, customerLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();

            List<User> users = DBService.getUsersByCustomerId(customerId);
            UsersResponse response = new UsersResponse(users);
            return Response.status(200).entity(response.toString()).build();
        }
        catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/{customer_id}/plans")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response getCustomerPlans(@PathParam("customer_id") String customerId) {
        try{
            String customerLogin = DBService.getCustomerDataById(customerId).getLogin();
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, customerLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();

            List<Plan> plans = DBService.getPlansByCustomerId(customerId);
            CustomerPlansResponse response = new CustomerPlansResponse(plans);
            return Response.status(200).entity(response.toString()).build();
        }
        catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/{customer_id}/subscriptions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response getCustomerSubscriptions(@PathParam("customer_id") String customerId) {
        try{
            String customerLogin = DBService.getCustomerDataById(customerId).getLogin();
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, customerLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();

            List<Subscription> subscriptions = DBService.getSubscriptionsByCustomerId(customerId);
            //SubscriptionsResponse response = new SubscriptionsResponse(subscriptions);
            return Response.status(200).entity(JsonMapper.toJson(subscriptions.stream().map(Subscription::getId).collect(Collectors.toList()), true)).build();
        }
        catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/{customer_id}/subscriptions/{plan_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response getCustomerSubscriptionsFilterPlan(@PathParam("customer_id") String customerId, @PathParam("plan_id") String planId) {
        try{
            String customerLogin = DBService.getCustomerDataById(customerId).getLogin();
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, customerLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();

            List<Subscription> subscriptions = DBService.getSubscriptionsByCustomerId(customerId);
            //SubscriptionsResponse response = new SubscriptionsResponse(subscriptions);
            return Response.status(200).entity(JsonMapper.toJson(subscriptions.stream()
                    .filter(it -> it.getData().getServicePlanId().toString().equals(planId))
                    .map(Subscription::getId).collect(Collectors.toList()), true)).build();
        }
        catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/{customer_id}/buy_plan/{plan_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response buyPlan(@PathParam("customer_id") String customerId, @PathParam("plan_id") String planId) {
        //TODO проверитьсовместимость планов
        try{
            String customerLogin = DBService.getCustomerDataById(customerId).getLogin();
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, customerLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();

            int customerMoney = DBService.getCustomerMoney(customerId);
            Plan plan = DBService.getPlan(planId);
            if(customerMoney < plan.getData().getCost())
                return Response.status(400).entity("Not enough money to buy plan").build();

            DBService.buyPlan(customerId, planId);
                return Response.status(200).entity("Success").build();
        }
        catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/{customer_id}/fillAccount/{money}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response fillAccount(@PathParam("customer_id") String customerId, @PathParam("money") int money) {
        if(money <= 0)
            return Response.status(400).entity("Value should be positive").build();
        try{
            String customerLogin = DBService.getCustomerDataById(customerId).getLogin();
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, customerLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();

            DBService.fillAccount(customerId, money);
            return Response.status(200).entity("Success").build();
        }
        catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/user/{user_id}/subscription/{subscription_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response assignSubscription(@PathParam("user_id") String userId, @PathParam("subscription_id") String subscriptionId) {
        try{
            String userCustomerLogin = DBService.getCustomerLoginByUserId(userId);
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, userCustomerLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();

            if(DBService.isSubscriptionAssigned(userId, subscriptionId))
                return Response.status(400).entity("Subscription is already assigned").build();
            Subscription subscription = DBService.getSubscription(subscriptionId);
            if(subscription.getData().getStatus() != SubscriptionStatus.DONE_STATUS)
                return Response.status(400).entity("Operation aborted. You try to use not confirmed subscription").build();
            int customerMoney = DBService.getCustomerMoney(subscription.getData().getCustomerId().toString());
            Plan plan = DBService.getPlan(subscription.getData().getServicePlanId().toString());
            if(customerMoney < plan.getData().getFeePerUnit())
                return Response.status(400).entity("Not enough money to buy").build();

            DBService.assignSubscription(userId, subscriptionId);
            return Response.status(200).entity("Success").build();
        }
        catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @DELETE
    @Path("/user/{user_id}/subscription/{subscription_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response reassignSubscription(@PathParam("user_id") String userId, @PathParam("subscription_id") String subscriptionId) {
        try{
            Subscription subscription = DBService.getSubscription(subscriptionId);
            String customerId = subscription.getData().getCustomerId().toString();
            String customerLogin = DBService.getCustomerDataById(customerId).getLogin();
            if(Objects.equals( /*sc.getUserPrincipal().getName()*/ null, customerLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();

            DBService.reassignSubscription(userId, subscriptionId);
            return Response.status(200).entity("Success").build();
        }
        catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed("CUSTOMER")
    @POST
    @Path("/user/{user_id}/role/{user_role}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static Response assignUserRole(@PathParam("user_id") String userId, @PathParam("user_role") String userRole) {
        try{
            /*
            String userCustomerLogin = DBService.getCustomerLoginByUserId(userId);
            if(Objects.equals( sc.getUserPrincipal().getName(), userCustomerLogin))
                return Response.status(400).entity("Operation aborted. Access denied").build();*/

            if(UserRole.valueOf(userRole) == null)
                return Response.status(400).entity("Role not found").build();

            DBService.assignUserRole(userId, UserRole.valueOf(userRole));
            return Response.status(200).entity("Success").build();
        }
        catch (IllegalArgumentException ex) {
            return Response.status(400).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
}