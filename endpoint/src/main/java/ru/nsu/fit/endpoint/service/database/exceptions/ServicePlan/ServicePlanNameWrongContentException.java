package ru.nsu.fit.endpoint.service.database.exceptions.ServicePlan;

public class ServicePlanNameWrongContentException extends ServicePlanException
{
    public ServicePlanNameWrongContentException()
    {
        super("Name can only consist of letters, numbers and spaces");
    }
}
